-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2022-06-28 08:47:54
-- 服务器版本： 5.7.23-log
-- PHP 版本： 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `jcms`
--

-- --------------------------------------------------------

--
-- 表的结构 `jcms_backend_user`
--

CREATE TABLE `jcms_backend_user` (
  `id` int(20) UNSIGNED NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `password_hash` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `password` varchar(511) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `phone` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '电话',
  `email` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '邮箱',
  `status` int(5) DEFAULT '0' COMMENT '状态 1-正常 2-异常 4-删除',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_at` datetime DEFAULT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='后台登录 用户表';

--
-- 转存表中的数据 `jcms_backend_user`
--

INSERT INTO `jcms_backend_user` (`id`, `username`, `password_hash`, `password`, `phone`, `email`, `status`, `create_at`, `update_at`) VALUES
(1, 'admin', '04DD865043F77F938212EB01925514BFDAEAE17233E656A6B4131E9F3D4467D32FFAE969648CF7FB1996DDEFFDF6C0FBA8953C2E1E65B612B350E19D9FDE6F2D2A9352132B96A9EA9A65ABA098A7777EC61A6312794F0FE7483B3977EA7D432864527667927180ECB0', 'QWIxMjM0NTY=', '18250850586', 'uduemc@163.com', 10, '2020-08-20 16:30:32', '2020-08-21 14:34:20');

-- --------------------------------------------------------

--
-- 表的结构 `jcms_category_type`
--

CREATE TABLE `jcms_category_type` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'ID',
  `name` varchar(127) NOT NULL COMMENT '分类类型名称',
  `remark` text COMMENT '说明',
  `config` text COMMENT '配置',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='全站分类系统总表';

-- --------------------------------------------------------

--
-- 表的结构 `jcms_repertory`
--

CREATE TABLE `jcms_repertory` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `user_type` int(11) NOT NULL DEFAULT '1' COMMENT '上传操作用户 1-后台',
  `aim_id` int(11) NOT NULL COMMENT '根据 user_type 类型记录的对应用户ID',
  `label_id` int(11) UNSIGNED DEFAULT '0' COMMENT '标签ID（0为未有标签）',
  `type` smallint(6) NOT NULL COMMENT '资源类型 1-图片 2-文件 3-Flash 4-视频 5-音频 6-外链图片 7-SSL -1-未知',
  `original_filename` varchar(255) NOT NULL COMMENT '真实文件名称',
  `link` varchar(255) DEFAULT '' COMMENT '外部资源链接地址',
  `suffix` varchar(255) DEFAULT '' COMMENT '后缀',
  `content_type` varchar(127) DEFAULT '' COMMENT '上传过来的 ContentType',
  `unidname` varchar(255) DEFAULT '' COMMENT '上传文件的唯一码，只对当前整站',
  `filepath` varchar(255) DEFAULT '' COMMENT '文件存储物理路径（相对路径）',
  `zoompath` varchar(255) DEFAULT '' COMMENT '缩放文件存储路径（相对路径）',
  `size` bigint(20) DEFAULT '0' COMMENT '大小',
  `pixel` varchar(50) DEFAULT '0' COMMENT '像素(图片使用)',
  `refuse` smallint(1) DEFAULT '0' COMMENT '0:非回收站；1：在回收站',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上传日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源模块表';

--
-- 转存表中的数据 `jcms_repertory`
--

INSERT INTO `jcms_repertory` (`id`, `user_type`, `aim_id`, `label_id`, `type`, `original_filename`, `link`, `suffix`, `content_type`, `unidname`, `filepath`, `zoompath`, `size`, `pixel`, `refuse`, `create_at`) VALUES
(6, 1, 1, 0, 1, 'img8.jpg', '', 'jpg', 'image/jpeg', 'f7bf8844d3c549d5814a72a585e3ec5d', 'upload\\repertory\\2022\\6\\13\\f7bf8844d3c549d5814a72a585e3ec5d.jpg', 'upload\\repertory\\2022\\6\\13\\f7bf8844d3c549d5814a72a585e3ec5d', 79337, '400X377', 0, '2022-06-13 11:39:47'),
(7, 1, 1, 0, 1, 'img6.jpg', '', 'jpg', 'image/jpeg', 'c174e9c0655c4752978811afa33667d1', 'upload\\repertory\\2022\\6\\13\\c174e9c0655c4752978811afa33667d1.jpg', 'upload\\repertory\\2022\\6\\13\\c174e9c0655c4752978811afa33667d1', 360874, '800X799', 0, '2022-06-13 11:39:51'),
(8, 1, 1, 1, 1, '002.jpg', '', 'jpg', 'image/jpeg', '0f94beec499d400fac932aee740cf2e5', 'upload\\repertory\\2022\\6\\13\\0f94beec499d400fac932aee740cf2e5.jpg', 'upload\\repertory\\2022\\6\\13\\0f94beec499d400fac932aee740cf2e5', 62467, '960X540', 0, '2022-06-13 11:39:55');

-- --------------------------------------------------------

--
-- 表的结构 `jcms_repertory_label`
--

CREATE TABLE `jcms_repertory_label` (
  `id` int(11) NOT NULL,
  `name` varchar(127) NOT NULL COMMENT '标签名',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源模块标签表';

--
-- 转存表中的数据 `jcms_repertory_label`
--

INSERT INTO `jcms_repertory_label` (`id`, `name`, `create_at`) VALUES
(1, '公共', '2022-06-10 11:38:25');

-- --------------------------------------------------------

--
-- 表的结构 `jcms_site`
--

CREATE TABLE `jcms_site` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `language_id` int(11) DEFAULT '1' COMMENT '语言版本ID',
  `status` smallint(3) NOT NULL DEFAULT '1' COMMENT '状态 0-未审核 1-开通 2-关闭 3-冻结 4-删除 ',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点表';

--
-- 转存表中的数据 `jcms_site`
--

INSERT INTO `jcms_site` (`id`, `language_id`, `status`, `create_at`) VALUES
(1, 3, 1, '2022-06-27 15:58:41');

-- --------------------------------------------------------

--
-- 表的结构 `jcms_sys_language`
--

CREATE TABLE `jcms_sys_language` (
  `id` int(11) NOT NULL,
  `name` varchar(10) NOT NULL COMMENT '名称',
  `lan_no` varchar(5) NOT NULL COMMENT '语言编码',
  `text` varchar(25) NOT NULL COMMENT '各个名称'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `jcms_sys_language`
--

INSERT INTO `jcms_sys_language` (`id`, `name`, `lan_no`, `text`) VALUES
(1, '中文', 'cn', '中文'),
(2, '繁体', 'hk', '繁体'),
(3, '英文', 'en', 'English'),
(4, '阿拉伯文', 'ar', 'العربية'),
(5, '德文', 'gr', 'Deutsch'),
(6, '西班牙文', 'es', 'Español'),
(7, '韩文', 'ko', '한국어'),
(8, '日文', 'ja', '日本語'),
(9, '法文', 'fr', 'Français'),
(10, '意大利文', 'it', 'Italiano'),
(11, '俄文', 'ru', 'русский '),
(12, '葡萄牙文', 'pt', 'Português');

-- --------------------------------------------------------

--
-- 表的结构 `jcms_s_category`
--

CREATE TABLE `jcms_s_category` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `category_type_id` int(11) UNSIGNED NOT NULL COMMENT '分类类型',
  `parent_id` int(11) UNSIGNED DEFAULT '0' COMMENT '父id',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '分类路径，使用[,]分隔，顶级为空',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '分类名称',
  `rewrite` varchar(255) DEFAULT '' COMMENT '重写链接',
  `order_num` int(11) DEFAULT '0' COMMENT '排序号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='分类';

-- --------------------------------------------------------

--
-- 表的结构 `jcms_s_category_quote`
--

CREATE TABLE `jcms_s_category_quote` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '唯一标识',
  `category_type_id` int(11) UNSIGNED NOT NULL COMMENT '分类类型ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `category_id` int(11) UNSIGNED NOT NULL COMMENT '分类ID',
  `aim_id` int(11) UNSIGNED NOT NULL COMMENT '目标ID',
  `order_num` int(11) DEFAULT '1' COMMENT '系统ID',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 表的结构 `jcms_s_config`
--

CREATE TABLE `jcms_s_config` (
  `id` int(11) NOT NULL,
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `site_banner` smallint(3) DEFAULT '0' COMMENT '是否开启整站统一banner 0-否',
  `site_seo` smallint(3) NOT NULL DEFAULT '1' COMMENT '是否使用整站同步 SEO 0-否 1-是 默认1',
  `is_main` smallint(3) DEFAULT '0' COMMENT '是否是主站点 0-不是 1-是 默认 0',
  `order_num` int(11) DEFAULT '1' COMMENT '排序',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='站点配置表';

-- --------------------------------------------------------

--
-- 表的结构 `jcms_s_logo`
--

CREATE TABLE `jcms_s_logo` (
  `id` int(11) NOT NULL,
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '语言ID',
  `type` smallint(3) DEFAULT '2' COMMENT '0-无logo 1-文字 2-图片',
  `title` varchar(128) DEFAULT '' COMMENT 'Logo 文字',
  `config` mediumtext COMMENT 'logo 配置属性',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='站点Logo配置表';

--
-- 转存表中的数据 `jcms_s_logo`
--

INSERT INTO `jcms_s_logo` (`id`, `site_id`, `type`, `title`, `config`, `create_at`) VALUES
(1, 1, 2, '网站Logo', '', '2022-06-27 17:20:25');

-- --------------------------------------------------------

--
-- 表的结构 `jcms_s_page`
--

CREATE TABLE `jcms_s_page` (
  `id` int(11) UNSIGNED NOT NULL,
  `site_id` int(11) UNSIGNED DEFAULT '0' COMMENT '站点ID,如果引导页，site_id必为0',
  `parent_id` int(11) UNSIGNED DEFAULT '0' COMMENT '父菜单ID',
  `system_id` int(11) UNSIGNED DEFAULT '0' COMMENT '系统ID',
  `vip_level` smallint(3) DEFAULT '-1' COMMENT '访问权限 默认 -1',
  `type` smallint(3) NOT NULL DEFAULT '1' COMMENT '页面类型 0-引导页 1-普通页 2-系统页 3-外链页 4-内链页',
  `name` varchar(128) NOT NULL COMMENT '名称',
  `status` smallint(6) DEFAULT '1' COMMENT '状态:0:不显示，1：显示',
  `rewrite` varchar(255) DEFAULT NULL COMMENT '重写链接',
  `url` varchar(255) DEFAULT NULL COMMENT '链接',
  `target` varchar(20) DEFAULT '_self' COMMENT '跳转方式',
  `config` mediumtext COMMENT '引导页配置',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='导航菜单';

-- --------------------------------------------------------

--
-- 表的结构 `jcms_s_repertory_quote`
--

CREATE TABLE `jcms_s_repertory_quote` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '语言ID',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `repertory_id` int(11) UNSIGNED NOT NULL COMMENT '资源ID',
  `type` int(11) NOT NULL COMMENT '类型(1:Logo,2:favorites图标)',
  `aim_id` int(11) UNSIGNED NOT NULL COMMENT '目标ID',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `config` mediumtext COMMENT '配置信息',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源引用表';

--
-- 转存表中的数据 `jcms_s_repertory_quote`
--

INSERT INTO `jcms_s_repertory_quote` (`id`, `site_id`, `parent_id`, `repertory_id`, `type`, `aim_id`, `order_num`, `config`, `create_at`) VALUES
(1, 1, 0, 6, 1, 1, 1, NULL, '2022-06-27 17:57:33');

--
-- 转储表的索引
--

--
-- 表的索引 `jcms_backend_user`
--
ALTER TABLE `jcms_backend_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- 表的索引 `jcms_category_type`
--
ALTER TABLE `jcms_category_type`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `jcms_repertory`
--
ALTER TABLE `jcms_repertory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `label_id` (`label_id`),
  ADD KEY `unidname` (`unidname`),
  ADD KEY `filepath` (`filepath`),
  ADD KEY `original_filename` (`original_filename`);

--
-- 表的索引 `jcms_repertory_label`
--
ALTER TABLE `jcms_repertory_label`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `jcms_site`
--
ALTER TABLE `jcms_site`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `jcms_sys_language`
--
ALTER TABLE `jcms_sys_language`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `jcms_s_category`
--
ALTER TABLE `jcms_s_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `category_type_id` (`category_type_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `path` (`path`),
  ADD KEY `name` (`name`),
  ADD KEY `rewrite` (`rewrite`);

--
-- 表的索引 `jcms_s_category_quote`
--
ALTER TABLE `jcms_s_category_quote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_type_id` (`category_type_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `aim_id` (`aim_id`);

--
-- 表的索引 `jcms_s_config`
--
ALTER TABLE `jcms_s_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `site_id` (`site_id`);

--
-- 表的索引 `jcms_s_logo`
--
ALTER TABLE `jcms_s_logo`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `jcms_s_page`
--
ALTER TABLE `jcms_s_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `rewrite` (`rewrite`);

--
-- 表的索引 `jcms_s_repertory_quote`
--
ALTER TABLE `jcms_s_repertory_quote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `language_id` (`site_id`),
  ADD KEY `aim_id` (`aim_id`),
  ADD KEY `repertory_id` (`repertory_id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `jcms_backend_user`
--
ALTER TABLE `jcms_backend_user`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `jcms_category_type`
--
ALTER TABLE `jcms_category_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- 使用表AUTO_INCREMENT `jcms_repertory`
--
ALTER TABLE `jcms_repertory`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=9;

--
-- 使用表AUTO_INCREMENT `jcms_repertory_label`
--
ALTER TABLE `jcms_repertory_label`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `jcms_site`
--
ALTER TABLE `jcms_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `jcms_sys_language`
--
ALTER TABLE `jcms_sys_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- 使用表AUTO_INCREMENT `jcms_s_category`
--
ALTER TABLE `jcms_s_category`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- 使用表AUTO_INCREMENT `jcms_s_category_quote`
--
ALTER TABLE `jcms_s_category_quote`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '唯一标识';

--
-- 使用表AUTO_INCREMENT `jcms_s_config`
--
ALTER TABLE `jcms_s_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `jcms_s_logo`
--
ALTER TABLE `jcms_s_logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `jcms_s_page`
--
ALTER TABLE `jcms_s_page`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `jcms_s_repertory_quote`
--
ALTER TABLE `jcms_s_repertory_quote`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
