package com.uduemc.jcms.generator;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.uduemc.jcms.config.upload.JcmsUploadConfig;
import com.uduemc.jcms.config.upload.conf.JCMSUploadConfigAudio;
import com.uduemc.jcms.config.upload.conf.JCMSUploadConfigFile;
import com.uduemc.jcms.config.upload.conf.JCMSUploadConfigFlash;
import com.uduemc.jcms.config.upload.conf.JCMSUploadConfigImage;
import com.uduemc.jcms.config.upload.conf.JCMSUploadConfigVideo;

@ActiveProfiles("dev")
@SpringBootTest
public class TestApplicationRun {

	@Autowired
	private JcmsUploadConfig jcmsUploadConfig;

	@Test
	void uploadConfig() {
		System.out.println(jcmsUploadConfig.getEntrance());

		System.out.println("=================================================");
		JCMSUploadConfigImage image = jcmsUploadConfig.getImage();
		System.out.println(Arrays.asList(image.getExt()));
		System.out.println(image.getRulSize().validSize());
		System.out.println(image.getRulSize().errorMessage());

		System.out.println("=================================================");
		JCMSUploadConfigFile file = jcmsUploadConfig.getFile();
		System.out.println(Arrays.asList(file.getExt()));
		System.out.println(file.getRulSize().validSize());
		System.out.println(file.getRulSize().errorMessage());

		System.out.println("=================================================");
		JCMSUploadConfigFlash flash = jcmsUploadConfig.getFlash();
		System.out.println(flash);
		System.out.println(Arrays.asList(flash.getExt()));
		System.out.println(flash.getRulSize().validSize());
		System.out.println(flash.getRulSize().errorMessage());

		System.out.println("=================================================");
		JCMSUploadConfigVideo video = jcmsUploadConfig.getVideo();
		System.out.println(Arrays.asList(video.getExt()));
		System.out.println(video.getRulSize().validSize());
		System.out.println(video.getRulSize().errorMessage());

		System.out.println("=================================================");
		JCMSUploadConfigAudio audio = jcmsUploadConfig.getAudio();
		System.out.println(Arrays.asList(audio.getExt()));
		System.out.println(audio.getRulSize().validSize());
		System.out.println(audio.getRulSize().errorMessage());
	}

//	@Test
//	void test() {
//		System.out.println(ContentCategory.type.def);
//	}

}
