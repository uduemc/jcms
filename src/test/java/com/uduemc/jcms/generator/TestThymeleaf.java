package com.uduemc.jcms.generator;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@SpringBootTest
class TestThymeleaf {

	@Autowired
	private TemplateEngine templateEngine;

	@Test
	void thymeleaf() {
		Context context = new Context();
		context.setVariable("name", "JCMS");
		String result = templateEngine.process("index.html", context);
		System.out.println(result);
	}

}
