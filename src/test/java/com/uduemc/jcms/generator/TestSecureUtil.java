package com.uduemc.jcms.generator;

import java.io.UnsupportedEncodingException;
import java.security.KeyPair;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

import cn.hutool.core.util.HexUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.asymmetric.SM2;

public class TestSecureUtil {

	@Test
	void ps() throws UnsupportedEncodingException {
		String content = "123456";

		KeyPair pair = SecureUtil.generateKeyPair("SM2");
		byte[] privateKey = pair.getPrivate().getEncoded();
		byte[] publicKey = pair.getPublic().getEncoded();

		final SM2 sm2 = SmUtil.sm2(privateKey, publicKey);
		String sign = sm2.signHex(HexUtil.encodeHexStr(content));

		// true
		boolean verify = sm2.verifyHex(HexUtil.encodeHexStr(content), sign);
		System.out.println(Arrays.toString(privateKey));
		System.out.println(Arrays.toString(publicKey));
		System.out.println(sign);
		System.out.println(verify);
	}
}
