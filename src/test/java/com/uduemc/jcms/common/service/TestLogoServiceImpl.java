package com.uduemc.jcms.common.service;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.jcms.JcmsApplication;
import com.uduemc.jcms.common.entity.Logo;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = JcmsApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestLogoServiceImpl {

	@Autowired
	private ILogoService logoServiceImpl;

	@Test
	public void createIfNotExists() throws Exception {
		Logo createIfNotExists = logoServiceImpl.createIfNotExists(1);
		System.out.println(createIfNotExists);
	}

}
