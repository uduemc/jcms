package com.uduemc.jcms.common.component.render;

import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.jcms.JcmsApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = JcmsApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestLogoRender {

	@Autowired
	JcmsRender jcmsRender;

	@Autowired
	LogoRender logoRender;

	@Test
	public void html() {
		jcmsRender.cacheSet("abc", "aa");
	}
}
