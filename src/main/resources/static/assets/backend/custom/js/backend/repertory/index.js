// 请求获得数据列表的存储变量
var repertoryResponseData = null;
(function(window, $) {
	$(function() {
		// 渲染
		// 初始化资源标签 combobox
		globalData.repertoryLabelInfos(function(labeles) {
			var labelComboboxData = [];
			labelComboboxData.push({
				id: 0,
				name: '无'
			});
			if (labeles.rows && labeles.rows.length > 0) {
				for (var i in labeles.rows) {
					var item = labeles.rows[i];
					labelComboboxData.push({
						id: item.id,
						name: item.name
					});
				}
			}
			$('#repertory-label_id').combobox({
				width: 220,
				valueField: 'id',
				textField: 'name',
				label: '标签',
				labelAlign: 'right',
				data: labelComboboxData
			});

			var templabelComboboxData = JSON.parse(JSON.stringify(labelComboboxData));
			templabelComboboxData.splice(0, 0, {
				id: -1,
				name: '全部'
			});
			$('#search-label_id').combobox({
				width: 300,
				valueField: 'id',
				multiple: true,
				textField: 'name',
				label: '标签：',
				labelWidth: 50,
				panelHeight: 200,
				labelAlign: 'right',
				data: templabelComboboxData,
				onChange: function(newValue) {
					var list = newValue.filter(element => {
						if (parseInt(element) === -1) {
							return false;
						}
						return true;
					});
					if (!list || list.length < 1) {
						list = ['-1'];
					}
					$('#search-label_id').combobox('setValues', list);
				}
			});

			// 渲染内容
			reloadReperoty();
		});
		// 搜索文件类型
		$('#search-type').combobox({
			width: 260,
			multiple: true,
			valueField: 'id',
			textField: 'name',
			label: '类型：',
			labelWidth: 50,
			panelHeight: 200,
			labelAlign: 'right',
			data: [
				{ id: '-1', name: '全部' },
				{ id: '1', name: '图片' },
				{ id: '2', name: '文件' },
				{ id: '3', name: 'Flash' },
				{ id: '4', name: '音频' },
				{ id: '5', name: '视频' },
				{ id: '6', name: '外链图片' }
			],
			onChange: function(newValue) {
				var list = newValue.filter(element => {
					if (parseInt(element) === -1) {
						return false;
					}
					return true;
				});
				if (!list || list.length < 1) {
					list = ['-1'];
				}
				$('#search-type').combobox('setValues', list);
			}
		});

		// 分页
		$('#repertory-pagination').pagination({
			total: 0,
			showPageList: false,
			showPageInfo: false,
			showRefresh: false,
			pageNumber: 1,
			pageSize: 12,
			onSelectPage: function() {
				reloadReperoty();
			}
		});

		// 展示页面
		$('body').mLoading('hide');
		$('.form-search').show();


		// 事件
		// 点击上传资源
		$('#uploadRepertory').linkbutton({
			onClick: function() {
				$("#uploadfile").click();
			}
		});
		// 点击添加外部链接按钮挂上事件
		$('#addOutsideLink').outsideLinkDialog({
			save: function() {
				reloadReperoty();
			}
		});
		// 上传动作
		$("#uploadfile").change(function() {
			var formData = new FormData();
			if ($("#uploadfile")[0].files.length < 1) {
				return;
			}
			var file = $("#uploadfile")[0].files[0];
			formData.append('file', file);
			request.upost(formData, {
				url: globalUrl.uploadUrl,
				success: function(rsp, data) {
					if (data && data.id && parseInt(data.id) > 0) {
						reloadReperoty();
					}
					$("#uploadfile").val('');
				}
			});
		});
	});
})(window, jQuery);

function searchdata() {
	$('#repertory-pagination').pagination('select', 1);
}

function resetdata() {
	$('#search-filename').textbox('setValue', '');
	$('#search-label_id').combobox('setValue', '-1');
	$('#search-type').combobox('setValue', '-1');
}

function editorRepertory(repertoryId) {
	if (!repertoryResponseData) {
		return;
	}
	var item = repertoryResponseData.rows.find(item => {
		return parseInt(item.id) === parseInt(repertoryId)
	})
	if (parseInt(item.type) === 6) {
		// 对外链图片资源进行编辑
		$.outsideLinkDialog({
			data: {
				id: parseInt(item.id),
				link: item.link,
				original_filename: item.originalFilename,
				label_id: item.labelId,
			},
			title: '编辑外链接图片',
			save: function() {
				reloadReperoty();
			}
		});
		return;
	} else {
		$('#repertory-id').textbox('setValue', '' + parseInt(item.id));
		$('#repertory-original_filename').textbox('setValue', '' + item.originalFilename);
		$('#repertory-label_id').combobox('setValue', '' + parseInt(item.labelId));
		$('#dlg-repertory .image-box').show();
		$('#dlg-repertory .image-box img').attr('src', item.imageSrc);
		var options = $('#dlg-repertory').dialog('options');
		options.onClose = function() {
			$('#repertory-id').textbox('setValue', '');
		}
		$('#dlg-repertory').dialog(options).dialog('center').dialog('open');
	}
}

// 删除
function deleteRepertory(repertoryId) {
	if (!repertoryResponseData) {
		return;
	}
	var item = repertoryResponseData.rows.find(item => {
		return parseInt(item.id) === parseInt(repertoryId)
	})
	if (!item) {
		return;
	}
	mylayer.confirm({
		message: '确定删除？',
		yes: function() {
			// 删除操作
			request.post({
				url: repertoryDeleteUrl,
				data: {
					id: item.id
				},
				success: function(rps, data) {
					if (data && parseInt(data) === 1) {
						reloadReperoty();
					}
				}
			});
		}
	});
}

// 更新
function saveRepertory() {
	$(function() {
		var id = $('#repertory-id').textbox('getValue');
		var originalFilename = $('#repertory-original_filename').textbox('getValue');
		var labelId = $('#repertory-label_id').combobox('getValue');

		var param = {
			id: id,
			originalFilename: originalFilename,
			labelId: labelId
		};

		request.jpost({
			url: repertoryUpdateUrl,
			data: param,
			success: function(rsp, data) {
				if (data && parseInt(data) === 1) {
					reloadReperoty();
					$('#dlg-repertory').dialog('close');
				}
			}
		});
	});
}

function reloadReperoty() {
	$(function() {
		var param = {
			filename: $.trim($('#search-filename').textbox('getValue')),
			// // aim_username:
			// // $.trim($('#search-aim_username').textbox('getValue')),
			// user_type :
			// $.trim($('#search-user_type').combobox('getValue')),
			label_id: $('#search-label_id').combobox('getValues').join(","),
			type: $('#search-type').combobox('getValues').join(","),
			is_refuse: 0,
			page: $('#repertory-pagination').pagination('options').pageNumber,
			rows: $('#repertory-pagination').pagination('options').pageSize
		};

		// console.log(param);
		// return;
		globalData.repertoryInfos(param, function(data) {
			// 存入变量
			repertoryResponseData = data
			// console.log(data);
			if (data && data.rows && data.rows.length > 0) {
				// 有数据的情况下进行渲染
				reloadReperotyRender(data.rows)
			} else {
				reloadReperotyRenderEmpty()
			}
			if (data.total > 12) {
				$('#repertory-pagination').show();
				var options = $('#repertory-pagination').pagination('options');
				options.total = parseInt(data.total);
				$('#repertory-pagination').pagination(options);
			} else {
				$('#repertory-pagination').hide();
			}
		});
	});
}

// 渲染空数据
function reloadReperotyRenderEmpty() {
	$('#gallery').html('');
	var html = '<div class="empty" style="height: 40px;position: relative;margin: 200px auto;font-size: 16px;color: #858585;">未查询到数据！</div>';
	$('#gallery').html(html);
}

// 渲染数据列表
function reloadReperotyRender(rows) {
	$('#gallery').html('');
	var html = '';
	for (var i in rows) {
		var item = rows[i];
		html += '<li data-id="' + item.id + '"><div class="item">';
		html += '<div class="image-box">';

		if (parseInt(item.type) === 0 || parseInt(item.type) === 6) {
			html += '<a href="' + item.imageSrc + '" class="img-a lery" title="' + item.originalFilename + '">';
			html += '<img src="' + item.imageSrc + '" class="image">';
			html += '</a>';
		} else {
			html += '<a class="img-a" title="' + item.originalFilename + '">';
			html += '<img src="' + item.imageSrc + '" class="image">';
			html += '</a>';
		}
		html += '</div>';
		html += '<div class="infos">';
		html += '<span class="title">' + item.originalFilename + '</span>';
		html += '<div class="labdes" style="padding-bottom: 4px;">';
		html += '<span>标签：<span class="each-label" data-labelid="' + item.labelId + '" >无</span></span>';
		html += '<span>';
		html += '<a class="des" href="javascript:void(0)" style="color: #337ab7;">属性</a>';
		html += '</span>';
		html += '</div>';
		html += '<div class="labdes" style="margin-top: 0;">';

		// $('#search-user_type').combobox
		html += '<span>来源：' + item.aimUsertype + '  ' + item.aimUsername + '</span>';
		html += '</div>';
		html += '<div class="operate">';
		if (parseInt(item.type) === 2) {
			if (['pdf'].indexOf(item.suffix) > -1) {
				// pdf 给予查看
				html += '<a href="' + item.linkUrl + '" target="_blank" style="color: #337ab7;">查看</a>';
				html += '&nbsp;&nbsp;&nbsp;&nbsp;';
			}
		} else {
			html += '<a href="' + item.linkUrl + '" target="_blank" style="color: #337ab7;">查看</a>';
			html += '&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		html += '<a href="javascript:void(0)" onclick="editorRepertory(' + item.id + ')" style="color: #337ab7;">编辑</a>';
		html += '&nbsp;&nbsp;&nbsp;&nbsp;';
		html += '<a href="javascript:void(0)" onclick="deleteRepertory(' + item.id + ')" style="color: #337ab7;">删除</a>';
		if (parseInt(item.type) !== 6) {
			html += '&nbsp;&nbsp;&nbsp;&nbsp;';
			html += '<a href="' + item.downloadUrl + '" target="_blank" style="color: #337ab7;">下载</a>';
		}
		html += '</div>';
		html += '</div>';
		html += '</div></li>';
	}

	$('#gallery').html(html);
	// 渲染完成后
	// 灯箱效果
	$('#gallery .image-box .lery').rebox();

	// 显示完整标题
	$('#gallery .item .infos .title').each(function() {
		$(this).tooltip({
			position: 'top',
			content: $(this).html(),
		});
	})

	// 显示详情
	$('#gallery .item .infos .labdes .des').each(function(index) {
		var item = rows[index];
		var content = '';
		if (parseInt(item.type) === 6) {
			content += '<div style="padding:2px 4px;">类型：外部链接图片</div>';
		} else if (parseInt(item.type) === 1) {
			content += '<div style="padding:2px 4px;">类型：图片</div>';
			content += '<div style="padding:2px 4px;">大小：' + func.makeByteSizeUnit(item.size) + '</div>';
			content += '<div style="padding:2px 4px;">尺寸：' + item.pixel + '</div>';
		} else if (parseInt(item.type) === 2) {
			content += '<div style="padding:2px 4px;">类型：文件</div>';
			content += '<div style="padding:2px 4px;">大小：' + func.makeByteSizeUnit(item.size) + '</div>';
		} else if (parseInt(item.type) === 3) {
			content += '<div style="padding:2px 4px;">类型：Flash</div>';
			content += '<div style="padding:2px 4px;">大小：' + func.makeByteSizeUnit(item.size) + '</div>';
		}
		else if (parseInt(item.type) === 4) {
			content += '<div style="padding:2px 4px;">类型：视频</div>';
			content += '<div style="padding:2px 4px;">大小：' + func.makeByteSizeUnit(item.size) + '</div>';
		} else if (parseInt(item.type) === 5) {
			content += '<div style="padding:2px 4px;">类型：音频</div>';
			content += '<div style="padding:2px 4px;">大小：' + func.makeByteSizeUnit(item.size) + '</div>';
		} else {
			console.log(item)
			return;
		}
		content += '<div style="padding:2px 4px;">创建时间：' + moment(item.createdAt).format('YYYY-MM-DD') + '</div>';
		$(this).tooltip({
			position: 'bottom',
			content: content,
		});
	});

	// 显示标签
	$('#gallery .each-label').each(function() {
		var _this = $(this);
		var labelId = _this.data('labelid');
		if (!labelId) {
			return;
		}
		globalData.repertoryLabelInfos(function(labeles) {
			var item = labeles.rows.find(ele => {
				return parseInt(ele.id) === parseInt(labelId);
			});
			if (item) {
				_this.html(item.name);
			}
		});
	});
}
