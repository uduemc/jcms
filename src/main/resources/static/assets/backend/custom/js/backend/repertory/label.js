$(function() {
	// 执行easyui渲染
	$('#repertory-label').datagrid({
		title : '资源标签（双击数据列编辑）',
		toolbar : '#repertory-label-tb',
		singleSelect : true,
		width : 600,
		minHeight : 250,
		url : repertoryLabelUrl,
		method : 'GET',
		pagination : true,
		columns : [ [ {
			field : 'id',
			title : 'ID',
			width : 40,
			align : 'center'
		}, {
			field : 'name',
			title : '标签名',
			width : 130,
			align : 'center'
		}, {
			field : 'createdAt',
			title : '创建时间',
			width : 120,
			align : 'center',
			formatter : function(val, row, ind) {
				return moment(val).format('YYYY-MM-DD');
			}
		} ] ],
		onClickRow : function(index, field, value) {
			window.event ? window.event.cancelBubble = true : e.stopPropagation();
		},
		onDblClickCell : function(index, field, value) {
			var rows = $('#repertory-label').datagrid('getRows');
			var row = rows[index];
			clickEdtLabel(row.id, row.name);
			window.event ? window.event.cancelBubble = true : e.stopPropagation();
		},
		loadFilter : function(data) {
			if (data && data.data) {
				return data.data;
			}
			return {
				rows : []
			};
		}
	});

	// 展示页面
	$('body').mLoading('hide');

	// 事件
//	$('.iframe-main').click(function() {
//		console.log('iframe-main');
//		return;
//		var bool = $('#repertory-label-dialog').parent().is(":hidden");
//		if (bool) {
//			$('#repertory-label').datagrid('unselectAll');
//		}
//	});
	
});

function clickAddLabel() {
	$(function() {
		$('#label-id').textbox("setValue", "");
		$('#label-name').textbox("setValue", "");
		$('#repertory-label-dialog').dialog({
			title : '新增标签',
		}).dialog('open');
	});
}

function clickDeleteLabel() {
	$(function() {
		var item = $('#repertory-label').datagrid('getSelected');
		if (!item) {
			mylayer.warning("选择下面一条数据进行删除！");
			return;
		}
		mylayer.confirm({
			message: '确定删除该标签？',
			yes: function () {
				// 删除操作
				request.post({
					url: labelDeleteUrl,
					data: {
						id: item.id
					},
					success: function(rsp, data){
						if(data && parseInt(data) === 1){
							$('#repertory-label').datagrid('reload');
						}
					}
				})
			}
		});
	});
}

function clickEdtLabel(id, name) {
	$(function() {
		$('#label-id').textbox("setValue", id);
		$('#label-name').textbox("setValue", name);
		$('#repertory-label-dialog').dialog({
			title : '编辑标签',
		}).dialog('open');
	});
}

function doSubmit() {
	$(function() {
		var id = $.trim($('#label-id').textbox("getValue"));
		var name = $.trim($('#label-name').textbox("getValue"));

		if (name.length < 1) {
			mylayer.warning('标签名不能为空！');
			return;
		}

		var param = {
			id : id,
			name : name
		};

		request.post({
			url : labelSaveUrl,
			data : param,
			success : function(rsp, data) {
				if (data && parseInt(data) === 1) {
					$('#repertory-label').datagrid('reload');
					$('#repertory-label-dialog').dialog('close');
				}
			}
		});
	});
}
