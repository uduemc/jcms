(function(window, $) {
	var globalUrl = {
		repertoryLabelInfosAllUrl: $('input[name="globalUrl-repertoryLabelInfosAllUrl"]').val(),
		repertoryInfosUrl: $('input[name="globalUrl-repertoryInfosUrl"]').val(),
		uploadUrl: $('input[name="globalUrl-uploadUrl"]').val()
	}
	window.globalUrl = window.globalUrl || globalUrl
	window.globalData = window.globalData || (function() {

		var repertoryLabelInfosData = {
			data: null,
			requesting: false
		}
		return {

			// 获取资源标签公用方法
			repertoryLabelInfos: function(callback) {
				var self = this;
				if (repertoryLabelInfosData.data) {
					if (typeof callback === 'function') {
						callback(repertoryLabelInfosData.data);
					}
					return;
				}
				// 是否在请求
				if (repertoryLabelInfosData.requesting) {
					// 循环等待
					setTimeout(function() {
						self.repertoryLabelInfos(callback);
					}, 100);
					return;
				}
				repertoryLabelInfosData.requesting = true;
				request.post({
					url: globalUrl.repertoryLabelInfosAllUrl,
					success: function(rsp, data) {
						repertoryLabelInfosData.data = data;
						repertoryLabelInfosData.requesting = false;
						self.repertoryLabelInfos(callback);
					}
				});
			},
			// 获取资源数据公共方法
			repertoryInfos: function(param, callback) {
				request.get({
					url: globalUrl.repertoryInfosUrl,
					data: param,
					success: function(rsp, data) {
						callback(data);
					}
				});
			},
			// 获取分类数据的方法
			categoryInfos: function(param, callback, completecallback) {
				request.get({
					url: globalUrl.categoryInfosUrl,
					data: param,
					success: function(rsp, data) {
						if (typeof callback === 'function') {
							callback(data);
						}
					},
					complete: function() {
						if (typeof completecallback === 'function') {
							completecallback();
						}
					}
				});
			}
		}
	})();
})(window, jQuery);