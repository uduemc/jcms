// 请求部分
var request = {
	/**
	 * ajax 请求 get json
	 */
	get : function(paramsObject) {
		// url, data, loadingObject, success, error, complete
		if (!paramsObject || !paramsObject.url) {
			return;
		}
		var url = paramsObject.url;
		var data = paramsObject.data ? paramsObject.data : null;
		var docObj = paramsObject.loadingObject ? paramsObject.loadingObject : null;
		var successback = paramsObject.success ? paramsObject.success : function() {
		};
		var errorback = paramsObject.error ? paramsObject.error : function() {
		};
		var completeback = paramsObject.complete ? paramsObject.complete : function() {
		};

		if (docObj && docObj.length > 0) {
			docObj.mLoading("show");
		}

		$.ajax({
			type : 'GET',
			url : url,
			data : data,
			dataType : 'json',
			success : function(response) {
				responseMsg(response);
				if (typeof successback === 'function') {
					successback(response, response.data)
				}
			},
			error : function() {
				if (typeof errorback === 'function') {
					errorback()
				}
			},
			complete : function(XHR, TS) {
				if (typeof completeback === 'function') {
					completeback(XHR, TS)
				}
				if (docObj && docObj.length > 0) {
					docObj.mLoading("hide");
				}
			}
		});
	},

	/**
	 * ajax 请求 post form
	 */
	post : function(paramsObject) {
		// url, data, loadingObject, success, error, complete
		if (!paramsObject || !paramsObject.url) {
			return;
		}
		var url = paramsObject.url;
		var data = paramsObject.data ? paramsObject.data : null;
		var docObj = paramsObject.loadingObject ? paramsObject.loadingObject : null;
		var successback = paramsObject.success ? paramsObject.success : function() {
		};
		var errorback = paramsObject.error ? paramsObject.error : function() {
		};
		var completeback = paramsObject.complete ? paramsObject.complete : function() {
		};

		if (docObj && docObj.length > 0) {
			docObj.mLoading("show");
		}
		$.ajax({
			type : 'POST',
			url : url,
			data : data,
			dataType : 'json',
			success : function(response) {
				responseMsg(response);
				if (typeof successback === 'function') {
					successback(response, response.data)
				}
			},
			error : function() {
				if (typeof errorback === 'function') {
					errorback()
				}
			},
			complete : function(XHR, TS) {
				if (typeof completeback === 'function') {
					completeback(XHR, TS)
				}
				if (docObj && docObj.length > 0) {
					docObj.mLoading("hide");
				}
			}
		});
	},
	/**
	 * ajax 请求 post json
	 */
	jpost : function(paramsObject) {
		// url, data, loadingObject, success, error, complete
		if (!paramsObject || !paramsObject.url) {
			return;
		}
		var url = paramsObject.url;
		var data = paramsObject.data ? paramsObject.data : null;
		var docObj = paramsObject.loadingObject ? paramsObject.loadingObject : null;
		var successback = paramsObject.success ? paramsObject.success : function() {
		};
		var errorback = paramsObject.error ? paramsObject.error : function() {
		};
		var completeback = paramsObject.complete ? paramsObject.complete : function() {
		};

		if (docObj && docObj.length > 0) {
			docObj.mLoading("show");
		}
		$.ajax({
			type : 'POST',
			url : url,
			contentType : "application/json; charset=utf-8",
			data : JSON.stringify(data),
			dataType : 'json',
			success : function(response) {
				responseMsg(response);
				if (typeof successback === 'function') {
					successback(response, response.data)
				}
			},
			error : function() {
				if (typeof errorback === 'function') {
					errorback()
				}
			},
			complete : function(XHR, TS) {
				if (typeof completeback === 'function') {
					completeback(XHR, TS)
				}
				if (docObj && docObj.length > 0) {
					docObj.mLoading("hide");
				}
			}
		});
	},
	/**
	 * ajax 请求 post 上传文件
	 */
	upost : function jpost(formData, paramsObject) {
		// url, data, loadingObject, success, error, complete
		if (!paramsObject || !paramsObject.url) {
			return;
		}
		var url = paramsObject.url;
		var data = paramsObject.data ? paramsObject.data : null;
		var docObj = paramsObject.loadingObject ? paramsObject.loadingObject : null;
		var successback = paramsObject.success ? paramsObject.success : function() {
		};
		var errorback = paramsObject.error ? paramsObject.error : function() {
		};
		var completeback = paramsObject.complete ? paramsObject.complete : function() {
		};

		for ( var i in data) {
			formData.append(i, data[i]);
		}

		if (docObj && docObj.length > 0) {
			docObj.mLoading("show");
		}

		$.ajax({
			url : url,
			type : "POST",
			dataType : "json",
			cache : false,
			data : formData,
			processData : false,// 不处理数据
			contentType : false, // 不设置内容类型
			success : function(response) {
				responseMsg(response);
				if (typeof successback === 'function') {
					successback(response, response.data)
				}
			},
			error : function() {
				if (typeof errorback === 'function') {
					errorback()
				}
			},
			complete : function(XHR, TS) {
				if (typeof completeback === 'function') {
					completeback(XHR, TS)
				}
				if (docObj && docObj.length > 0) {
					docObj.mLoading("hide");
				}
			}
		});
	}

}

function responseMsg(response) {
	if (response && response.message && response.message.message) {
		var msg = response.message.message;
		var icon;
		if (response.message.type === 'success') {
			icon = 1;
		} else if (response.message.type === 'error') {
			icon = 2;
		} else if (response.message.type === 'warning') {
			icon = 3;
		}
		if (msg && icon) {
			layer.ready(function() {
				layer.msg(msg, {
					icon : icon,
					time : 2000
				}, function() {
				});
			});
		} else if (msg) {
			layer.ready(function() {
				layer.msg(msg, {
					time : 2000
				}, function() {
				});
			});
		}
	}
}

var mylayer = {
	error : function(msg) {
		layer.msg(msg, {
			icon : 2,
			time : 2000
		});
	},
	warning : function(msg) {
		layer.msg(msg, {
			icon : 0,
			time : 2000
		});
	},
	confirm : function(obj) {
		var param = Object.assign({}, {
			title : '提示',
			message : '提示内容',
			btn : [ '确定', '取消' ],
			yes : function() {
			},
			yesClose : true,
			cancel : function() {
			},
			cancelClose : true
		}, obj)
		// 询问框
		layer.confirm(param.message, {
			title : param.title,
			btn : param.btn
		// 按钮
		}, function(index) {
			if (typeof param.yes === 'function') {
				param.yes(index);
			}
			if (param.yesClose) {
				layer.close(index);
			}
		}, function(index) {
			if (typeof param.cancel === 'function') {
				param.cancel(index);
			}
			if (param.cancelClose) {
				layer.close(index);
			}
		});
	}
}

var func = {
	// 转换为大小单位
	makeByteSizeUnit : function(value) {
		if (value < 1024) {
			return value + 'b'
		} else if (value < 1024 * 1024) {
			return parseFloat(value / 1024).toFixed(1) + 'Kb'
		} else if (value < 1024 * 1024 * 1024) {
			return parseFloat(value / (1024 * 1024)).toFixed(1) + 'M'
		} else {
			return parseFloat(value / (1024 * 1024 * 1024)).toFixed(1) + 'G'
		}
	}
};
