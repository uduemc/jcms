$(function() {
	var logoJson = $("#logoJson").text();
	logoJson = JSON.parse(logoJson);
	if (logoJson.repertoryQuote) {
		var repertoryAimUsername = logoJson.repertoryQuote.repertoryAimUsername
		if (repertoryAimUsername && repertoryAimUsername.id) {
			$('#logoImg1').textbox('setValue', repertoryAimUsername.originalFilename);
			$('#logoImg1-repertoryId').val(repertoryAimUsername.id);
			if (repertoryAimUsername.imageSrc) {
				$('#logoImg1-img').attr('src', repertoryAimUsername.imageSrc);
			}
		}
	}
	if (logoJson.jcmsSLogo) {
		var config = logoJson.jcmsSLogo ? logoJson.jcmsSLogo.config : '';
		if (config) {
			config = JSON.parse(config);
			if (config.imgAlt) {
				$('#logoImgAlt').textbox('setValue', config.imgAlt);
			}
			if (config.imgTitle) {
				$('#logoImgTitle').textbox('setValue', config.imgTitle);
			}
		}
	}

	$('#choose-repertory').on('click', function() {
		$.jcmsRepertory({
			total: 1,
			type: [1, 6],
			choose: function(repertories) {
				if (repertories && repertories.length > 0) {
					var repertory = repertories[0];
					$('#logoImg1').textbox('setValue', repertory.originalFilename);
					$('#logoImg1-repertoryId').val(repertory.id);
					$('#logoImg1-img').attr('src', repertory.imageSrc);
				}
			}
		}).show();
	});

	$('#logo-save').on('click', function() {
		var repertoryId = $.trim($('#logoImg1-repertoryId').val());
		var imgAlt = $.trim($('#logoImgAlt').textbox('getValue'));
		var imgTitle = $.trim($('#logoImgTitle').textbox('getValue'));
		if (repertoryId == "" || isNaN(parseInt(repertoryId)) || parseInt(repertoryId) < 1) {
			repertoryId = 0;
		}

		var param = {
			repertoryId: repertoryId,
			logoConfig: JSON.stringify({
				imgAlt: imgAlt,
				imgTitle: imgTitle
			})
		}

		request.post({
			url: $('input[name="save-logo-url"]').val(),
			data: param,
			success: function(rsp, data) {
				if (data && data.jcmsSLogo && data.jcmsSLogo.id) {
					window.location.reload();
				}
			}
		})
	});

	// 展示页面
	$('body').mLoading('hide');

});