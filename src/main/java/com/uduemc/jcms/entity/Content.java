package com.uduemc.jcms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 整体系统内容数据
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("content")
public class Content implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 标题
     */
    private String title;

    /**
     * 类型 1-富文本 0-系统
     */
    private Integer type;

    /**
     * 状态 1-正常 2-隐藏 4-删除
     */
    private Integer status;

    /**
     * 内容
     */
    private String content;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * icon 图标
     */
    private String icon;

    /**
     * 配置
     */
    private String conf;

    /**
     * 创建时间
     */
    private LocalDateTime createdAt;


}
