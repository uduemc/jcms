package com.uduemc.jcms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 多对多分类引用数据
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("categories_quote")
public class CategoriesQuote implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 分类数据表区分
     */
    private String typeTable;

    /**
     * 分类数据表 分类 区分
     */
    private Integer type;

    /**
     * 经营分类ID
     */
    private Integer categoryId;

    /**
     * 对应的关联ID
     */
    private Integer aimId;

    /**
     * 排序号
     */
    private Integer orderNum;

    /**
     * 创建时间
     */
    private LocalDateTime createdAt;


}
