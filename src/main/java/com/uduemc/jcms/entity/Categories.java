package com.uduemc.jcms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 整体系统分类
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("categories")
public class Categories implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 分类数据表区分
     */
    private String typeTable;

    /**
     * 分类数据表 分类 区分
     */
    private Integer type;

    /**
     * 父id
     */
    private Integer parentId;

    /**
     * 分类路径 顶级为空
     */
    private String path;

    /**
     * 系列名称
     */
    private String name;

    /**
     * 排序号
     */
    private Integer orderNum;

    /**
     * 创建时间
     */
    private LocalDateTime createdAt;


}
