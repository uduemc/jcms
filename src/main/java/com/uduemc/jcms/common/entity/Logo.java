package com.uduemc.jcms.common.entity;

import com.uduemc.jcms.generate.entity.JcmsSLogo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class Logo {

	JcmsSLogo jcmsSLogo;

	RepertoryQuote repertoryQuote;

}
