package com.uduemc.jcms.common.entity;

import com.uduemc.jcms.backend.vo.JcmsRepertoryAimUsername;
import com.uduemc.jcms.generate.entity.JcmsRepertory;
import com.uduemc.jcms.generate.entity.JcmsRepertoryLabel;
import com.uduemc.jcms.generate.entity.JcmsSRepertoryQuote;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class RepertoryQuote {

	JcmsSRepertoryQuote repertoryQuote;
	JcmsRepertory repertory;
	JcmsRepertoryAimUsername repertoryAimUsername;
	JcmsRepertoryLabel repertoryLabel;

}
