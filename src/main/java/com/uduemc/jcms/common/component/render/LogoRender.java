package com.uduemc.jcms.common.component.render;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.uduemc.jcms.common.service.ILogoService;

import cn.hutool.core.util.StrUtil;

@Component
public class LogoRender {

	public final static String KEY = LogoRender.class.toString();

	@Autowired
	private JcmsRender jcmsRender;

	@Autowired
	ApplicationContext applicationContext;

	public String html() {

		String cache = jcmsRender.cacheGet(KEY);
		if (StrUtil.isNotBlank(cache)) {
			return cache;
		}

		ILogoService logoServiceImpl = applicationContext.getBean("logoServiceImpl", ILogoService.class);
		logoServiceImpl.createIfNotExists(0);

		return "<div class=\"xg_logo p1700mf1700-08d08b5990310810f\">" + "		<h1>\r\n" + "			<a href=\"/\">\r\n"
				+ "				<img data-src=\"/Images/logo.png?t=20220609112138\" data-src-sm=\"/Images/m_logo.png?t=20220609112138\" src=\"http://sinoorchard.com/Images/logo.png\" alt=\"Xiamen Sino Orchard Industry Co Ltd\" title=\"Xiamen Sino Orchard Industry Co Ltd\">\r\n"
				+ "			</a>\r\n" + "		</h1>\r\n" + "	</div>";
	}
}
