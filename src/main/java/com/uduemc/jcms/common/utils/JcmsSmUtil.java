package com.uduemc.jcms.common.utils;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.SM2;

public class JcmsSmUtil {

	// 私钥
	protected final static String PrivateKey = "MIGTAgEAMBMGByqGSM49AgEGCCqBHM9VAYItBHkwdwIBAQQgPdElNkPU9lAweXOKgMRjyAZIc76GwGHEBy75Izv1jF2gCgYIKoEcz1UBgi2hRANCAAS3W9FOPkcm3H3/nQe/KSezhJQqqzOPyBRRoMcvzDgK6qAVX4mFBlRyBHC9qT/zEIambKbKiRbcC9xpCnyxNaP3";

	// 公钥
	public final static String PublicKey = "MFkwEwYHKoZIzj0CAQYIKoEcz1UBgi0DQgAEt1vRTj5HJtx9/50Hvykns4SUKqszj8gUUaDHL8w4CuqgFV+JhQZUcgRwvak/8xCGpmymyokW3AvcaQp8sTWj9w==";

	// 创建 sm2 对象
	protected final static SM2 SM2Object = SmUtil.sm2(PrivateKey, PublicKey);

	// 加密
	public static String encrypt(String text) {
		return SM2Object.encryptBcd(text, KeyType.PublicKey);
	}

	// 解密
	public static String decrypt(String text) {
		return StrUtil.utf8Str(SM2Object.decryptFromBcd(text, KeyType.PrivateKey));
	}

}
