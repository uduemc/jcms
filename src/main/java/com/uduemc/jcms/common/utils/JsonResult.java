package com.uduemc.jcms.common.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 
 * @Title: JsonResult.java
 * @Package com.uduemc.utils
 * @Description: 自定义响应数据结构 这个类是提供给门户，ios，安卓，微信商城用的
 *               门户接受此类数据后需要使用本类的方法转换成对于的数据类型格式（类，或者list） 其他自行处理 200：表示成功
 *               500：表示错误，错误信息在msg字段中 501：bean验证错误，不管多少个错误都以map形式返回
 *               502：拦截器拦截到用户token出错 555：异常抛出信息
 * @author uduemc
 * @date 2016年4月22日 下午8:33:36
 * @version V1.0
 */
@Data
@Accessors(chain = true)
public class JsonResult {

	// 响应业务状态
	private int code;

	// 响应message消息
	private Map<String, Object> message;

	// 响应messageBox消息
	private Map<String, Object> messageBox;

	// 提示通知消息
	private Map<String, Object> notification;

	// 提示通知消息
	private Map<String, Object> console;

	// 响应中的数据
	private Object data;

	public JsonResult() {
		super();
	}

	// 验证是否正常返回200
	public static boolean code200(JsonResult jsonResult) {
		return jsonResult.getCode() == 200;
	}

	// 未登录错误
	public static JsonResult noLogin(String msg) {
		Map<String, Object> message = new HashMap<>();
		message.put("type", "error");
		message.put("showClose", "true");
		message.put("message", msg);
		JsonResult result = new JsonResult();
		result.setCode(600).setMessage(message);
		return result;
	}

	// 未登录错误
	public static JsonResult noLogin() {
		JsonResult result = new JsonResult();
		result.setCode(600);
		return result;
	}

	// success
	public static JsonResult messageBoxSuccess(String title, String msg) {
		return messageBox(title, msg, "success", null);
	}

	// info
	public static JsonResult messageBoxInfo(String title, String msg) {
		return messageBox(title, msg, "info", null);
	}

	// warning
	public static JsonResult messageBoxWarning(String title, String msg) {
		return messageBox(title, msg, "Warning", null);
	}

	// error
	public static JsonResult messageBoxError(String title, String msg) {
		return messageBox(title, msg, "error", null);
	}

	// success
	public static JsonResult messageBoxSuccess(String title, String msg, Object data) {
		return messageBox(title, msg, "success", data);
	}

	// info
	public static JsonResult messageBoxInfo(String title, String msg, Object data) {
		return messageBox(title, msg, "info", data);
	}

	// warning
	public static JsonResult messageBoxWarning(String title, String msg, Object data) {
		return messageBox(title, msg, "Warning", data);
	}

	// error
	public static JsonResult messageBoxError(String title, String msg, Object data) {
		return messageBox(title, msg, "error", data);
	}

	public static JsonResult messageBox(String title, String msg, String type, Object data) {
		Map<String, Object> messageBox = new HashMap<>();
		messageBox.put("title", title);
		messageBox.put("message", msg);
		messageBox.put("type", type);
		JsonResult result = new JsonResult();
		result.setCode(200).setMessageBox(messageBox).setData(data);
		return result;
	}

	// 非法操作
	public static JsonResult illegal() {
		return messageError("非法操作！");
	}

	// 请联系管理员
	public static JsonResult assistance() {
		return messageError("操作失败，请联系管理员！");
	}

	// 错误
	public static JsonResult messageError(String msg, Object data) {
		Map<String, Object> message = new HashMap<>();
		message.put("type", "error");
		message.put("showClose", "true");
		message.put("message", msg);
		JsonResult result = new JsonResult();
		result.setCode(500).setMessage(message).setData(data);
		return result;
	}

	// 错误
	public static JsonResult messageError(String msg) {
		Map<String, Object> message = new HashMap<>();
		message.put("type", "error");
		message.put("showClose", "true");
		message.put("message", msg);
		JsonResult result = new JsonResult();
		result.setCode(500).setMessage(message);
		return result;
	}

	// 错误
	public static JsonResult messageError(BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			for (ObjectError objectError : bindingResult.getAllErrors()) {
				return messageError(objectError.getDefaultMessage());
			}
		}
		return null;
	}

	// 警告
	public static JsonResult messageWarning(String msg, Object data) {
		Map<String, Object> message = new HashMap<>();
		message.put("type", "warning");
		message.put("showClose", "true");
		message.put("message", msg);
		JsonResult result = new JsonResult();
		result.setCode(500).setMessage(message).setData(data);
		return result;
	}

	// 警告
	public static JsonResult messageWarning(String msg) {
		Map<String, Object> message = new HashMap<>();
		message.put("type", "warning");
		message.put("showClose", "true");
		message.put("message", msg);
		JsonResult result = new JsonResult();
		result.setCode(500).setMessage(message);
		return result;
	}

	// 成功
	public static JsonResult messageSuccess(String msg, Object data) {
		Map<String, Object> message = new HashMap<>();
		message.put("type", "success");
		message.put("showClose", "true");
		message.put("message", msg);
		JsonResult result = new JsonResult();
		result.setCode(200).setMessage(message).setData(data);
		return result;
	}

	// 成功
	public static JsonResult messageSuccess(Object data) {
		return messageSuccess("保存成功！", data);
	}

	// 成功
	public static JsonResult messageSuccess() {
		return messageSuccess("保存成功！");
	}

	// 成功
	public static JsonResult messageSuccess(String msg) {
		Map<String, Object> message = new HashMap<>();
		message.put("type", "success");
		message.put("showClose", "true");
		message.put("message", msg);
		JsonResult result = new JsonResult();
		result.setCode(200).setMessage(message);
		return result;
	}

	// console.error()
	public static JsonResult consoleError(String msg) {
		Map<String, Object> console = new HashMap<>();
		console.put("error", msg);
		JsonResult result = new JsonResult();
		result.setCode(500).setConsole(console);
		return result;
	}

	// console.error()
	public static JsonResult consoleError(String msg, Object data) {
		Map<String, Object> console = new HashMap<>();
		console.put("error", msg);
		JsonResult result = new JsonResult();
		result.setCode(500).setConsole(console).setData(data);
		return result;
	}

	public static JsonResult ok() {
		JsonResult result = new JsonResult();
		result.setCode(200);
		return result;
	}

	public static JsonResult ok(Object data) {
		JsonResult result = new JsonResult();
		result.setCode(200).setData(data);
		return result;
	}

}
