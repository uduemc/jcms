package com.uduemc.jcms.common.service;

import com.uduemc.jcms.common.entity.Logo;
import com.uduemc.jcms.common.utils.JsonResult;

public interface ILogoService {
	Logo createIfNotExists();

	Logo createIfNotExists(int siteId);

	JsonResult save(int repertoryId, String logoConfig);

	JsonResult save(int siteId, int repertoryId, String logoConfig);
}
