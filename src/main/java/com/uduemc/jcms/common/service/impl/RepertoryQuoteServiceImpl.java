package com.uduemc.jcms.common.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.uduemc.jcms.backend.vo.JcmsRepertoryAimUsername;
import com.uduemc.jcms.common.entity.RepertoryQuote;
import com.uduemc.jcms.common.service.IRepertoryQuoteService;
import com.uduemc.jcms.component.RepertoryUtil;
import com.uduemc.jcms.generate.entity.JcmsBackendUser;
import com.uduemc.jcms.generate.entity.JcmsRepertory;
import com.uduemc.jcms.generate.entity.JcmsRepertoryLabel;
import com.uduemc.jcms.generate.entity.JcmsSRepertoryQuote;
import com.uduemc.jcms.generate.service.IJcmsBackendUserService;
import com.uduemc.jcms.generate.service.IJcmsRepertoryLabelService;
import com.uduemc.jcms.generate.service.IJcmsRepertoryService;
import com.uduemc.jcms.generate.service.IJcmsSRepertoryQuoteService;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;

@Service
public class RepertoryQuoteServiceImpl implements IRepertoryQuoteService {

	@Autowired
	IJcmsRepertoryService jcmsRepertoryServiceImpl;

	@Autowired
	IJcmsSRepertoryQuoteService jcmsSRepertoryQuoteServiceImpl;

	@Autowired
	IJcmsBackendUserService jcmsBackendUserServiceImpl;

	@Autowired
	IJcmsRepertoryLabelService jcmsRepertoryLabelServiceImpl;

	@Autowired
	RepertoryUtil repertoryUtil;

	@Override
	public List<RepertoryQuote> find(int siteId, int type, int aimId) {
		LambdaQueryWrapper<JcmsSRepertoryQuote> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(JcmsSRepertoryQuote::getSiteId, siteId).eq(JcmsSRepertoryQuote::getType, type).eq(JcmsSRepertoryQuote::getAimId, aimId);
		queryWrapper.orderByDesc(JcmsSRepertoryQuote::getOrderNum).orderByDesc(JcmsSRepertoryQuote::getId);
		List<JcmsSRepertoryQuote> jcmsSRepertoryQuoteList = jcmsSRepertoryQuoteServiceImpl.list(queryWrapper);
		if (CollUtil.isEmpty(jcmsSRepertoryQuoteList)) {
			return null;
		}

		List<RepertoryQuote> result = new ArrayList<>();

		for (JcmsSRepertoryQuote jcmsSRepertoryQuote : jcmsSRepertoryQuoteList) {
			RepertoryQuote repertoryQuote = new RepertoryQuote();
			repertoryQuote.setRepertoryQuote(jcmsSRepertoryQuote);

			JcmsRepertory jcmsRepertory = jcmsRepertoryServiceImpl.getById(jcmsSRepertoryQuote.getRepertoryId());
			if (jcmsRepertory != null) {
				JcmsRepertoryAimUsername repertoryAimUsername = makeJcmsRepertoryAimUsername(jcmsRepertory);
				repertoryQuote.setRepertory(jcmsRepertory).setRepertoryAimUsername(repertoryAimUsername);
				Integer labelId = jcmsRepertory.getLabelId();
				if (labelId != null && labelId.intValue() > 0) {
					JcmsRepertoryLabel jcmsRepertoryLabel = jcmsRepertoryLabelServiceImpl.getById(labelId);
					repertoryQuote.setRepertoryLabel(jcmsRepertoryLabel);
				}
			}

			result.add(repertoryQuote);
		}

		return result;
	}

	@Override
	public RepertoryQuote findOne(int siteId, int type, int aimId) {
		LambdaQueryWrapper<JcmsSRepertoryQuote> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(JcmsSRepertoryQuote::getSiteId, siteId).eq(JcmsSRepertoryQuote::getType, type).eq(JcmsSRepertoryQuote::getAimId, aimId);
		queryWrapper.orderByDesc(JcmsSRepertoryQuote::getOrderNum).orderByDesc(JcmsSRepertoryQuote::getId);
		JcmsSRepertoryQuote jcmsSRepertoryQuote = jcmsSRepertoryQuoteServiceImpl.getOne(queryWrapper);
		if (jcmsSRepertoryQuote == null) {
			return null;
		}

		RepertoryQuote repertoryQuote = new RepertoryQuote();
		repertoryQuote.setRepertoryQuote(jcmsSRepertoryQuote);

		JcmsRepertory jcmsRepertory = jcmsRepertoryServiceImpl.getById(jcmsSRepertoryQuote.getRepertoryId());
		if (jcmsRepertory != null) {
			JcmsRepertoryAimUsername repertoryAimUsername = makeJcmsRepertoryAimUsername(jcmsRepertory);
			repertoryQuote.setRepertory(jcmsRepertory).setRepertoryAimUsername(repertoryAimUsername);
			Integer labelId = jcmsRepertory.getLabelId();
			if (labelId != null && labelId.intValue() > 0) {
				JcmsRepertoryLabel jcmsRepertoryLabel = jcmsRepertoryLabelServiceImpl.getById(labelId);
				repertoryQuote.setRepertoryLabel(jcmsRepertoryLabel);
			}
		}

		return repertoryQuote;

	}

	@Override
	public JcmsRepertoryAimUsername makeJcmsRepertoryAimUsername(JcmsRepertory jcmsRepertory) {
		JcmsRepertoryAimUsername repertoryAimUsername = new JcmsRepertoryAimUsername();
		BeanUtil.copyProperties(jcmsRepertory, repertoryAimUsername);

		// 获取用户名
		if (jcmsRepertory.getUserType().intValue() == 1) {
			repertoryAimUsername.setAimUsertype("后台用户");
			JcmsBackendUser backendUser = jcmsBackendUserServiceImpl.getById(jcmsRepertory.getAimId());
			repertoryAimUsername.setAimUsername(backendUser == null ? "用户不存在" : backendUser.getUsername());

		} else {
			throw new RuntimeException("未能识别对应的用户类型 userType: " + jcmsRepertory.getUserType());
		}

		// 获取前台的访问链接
		repertoryAimUsername.setImageSrc(repertoryUtil.imageSrc(jcmsRepertory));
		repertoryAimUsername.setLinkUrl(repertoryUtil.linkUrl(jcmsRepertory));
		repertoryAimUsername.setDownloadUrl(repertoryUtil.downloadUrl(jcmsRepertory));

		return repertoryAimUsername;
	}

}
