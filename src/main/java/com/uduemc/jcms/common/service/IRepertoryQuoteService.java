package com.uduemc.jcms.common.service;

import java.util.List;

import com.uduemc.jcms.backend.vo.JcmsRepertoryAimUsername;
import com.uduemc.jcms.common.entity.RepertoryQuote;
import com.uduemc.jcms.generate.entity.JcmsRepertory;

public interface IRepertoryQuoteService {

	List<RepertoryQuote> find(int siteId, int type, int aimId);

	RepertoryQuote findOne(int siteId, int type, int aimId);

	JcmsRepertoryAimUsername makeJcmsRepertoryAimUsername(JcmsRepertory repertory);
}
