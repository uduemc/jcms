package com.uduemc.jcms.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.jcms.backend.component.SiteLanguageForm;
import com.uduemc.jcms.common.entity.Logo;
import com.uduemc.jcms.common.entity.RepertoryQuote;
import com.uduemc.jcms.common.entity.json.ImgTagConfig;
import com.uduemc.jcms.common.mapper.LogoMapper;
import com.uduemc.jcms.common.service.ILogoService;
import com.uduemc.jcms.common.service.IRepertoryQuoteService;
import com.uduemc.jcms.common.utils.JsonResult;
import com.uduemc.jcms.generate.entity.JcmsSLogo;
import com.uduemc.jcms.generate.entity.JcmsSRepertoryQuote;
import com.uduemc.jcms.generate.service.IJcmsSLogoService;
import com.uduemc.jcms.generate.service.IJcmsSRepertoryQuoteService;

@Service
public class LogoServiceImpl implements ILogoService {

	@Autowired
	LogoMapper logoMapper;

	@Autowired
	IJcmsSLogoService jcmsSLogoServiceImpl;

	@Autowired
	IJcmsSRepertoryQuoteService jcmsSRepertoryQuoteServiceImpl;

	@Autowired
	IRepertoryQuoteService repertoryQuoteServiceImpl;

	@Autowired
	SiteLanguageForm siteLanguageForm;

	@Autowired
	ObjectMapper objectMapper;

	@Override
	public Logo createIfNotExists() {
		return createIfNotExists(siteLanguageForm.getSiteLanguage().getSite().getId());
	}

	@Override
	public Logo createIfNotExists(int siteId) {
		Logo logo = logoMapper.selectBySiteId(siteId);
		if (logo == null) {
			logo = new Logo();
			JcmsSLogo jcmsSLogo = new JcmsSLogo();
			jcmsSLogo.setType(2);
			jcmsSLogo.setSiteId(siteId);
			jcmsSLogo.setTitle("网站Logo");
			jcmsSLogo.setConfig("");
			jcmsSLogoServiceImpl.save(jcmsSLogo);
			logo.setJcmsSLogo(jcmsSLogo);
		}

		RepertoryQuote repertoryQuote = repertoryQuoteServiceImpl.findOne(siteId, 1, logo.getJcmsSLogo().getId());
		logo.setRepertoryQuote(repertoryQuote);

		return logo;
	}

	@Override
	public JsonResult save(int repertoryId, String logoConfig) {
		return save(siteLanguageForm.getSiteLanguage().getSite().getId(), repertoryId, logoConfig);
	}

	@Transactional
	@Override
	public JsonResult save(int siteId, int repertoryId, String logoConfig) {
		Logo logo = createIfNotExists(siteId);
		JcmsSLogo jcmsSLogo = logo.getJcmsSLogo();

		try {
			ImgTagConfig imgTagConfig = objectMapper.readValue(logoConfig, ImgTagConfig.class);
			jcmsSLogo.setConfig(objectMapper.writeValueAsString(imgTagConfig));
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		jcmsSLogoServiceImpl.updateById(jcmsSLogo);

		RepertoryQuote repertoryQuote = logo.getRepertoryQuote();
		JcmsSRepertoryQuote jcmsSRepertoryQuote = repertoryQuote != null ? repertoryQuote.getRepertoryQuote() : null;
		if (repertoryId > 0) {
			if (jcmsSRepertoryQuote == null) {
				// 插入
				jcmsSRepertoryQuote = new JcmsSRepertoryQuote();
				jcmsSRepertoryQuote.setAimId(jcmsSLogo.getId());
				jcmsSRepertoryQuote.setConfig("");
				jcmsSRepertoryQuote.setOrderNum(1);
				jcmsSRepertoryQuote.setParentId(0);
				jcmsSRepertoryQuote.setRepertoryId(repertoryId);
				jcmsSRepertoryQuote.setSiteId(siteId);
				jcmsSRepertoryQuote.setType(1);
				jcmsSRepertoryQuoteServiceImpl.save(jcmsSRepertoryQuote);
			} else {
				// 修改
				jcmsSRepertoryQuote.setRepertoryId(repertoryId);
				jcmsSRepertoryQuoteServiceImpl.updateById(jcmsSRepertoryQuote);
			}
		} else {
			if (jcmsSRepertoryQuote != null) {
				// 删除
				jcmsSRepertoryQuoteServiceImpl.removeById(jcmsSRepertoryQuote.getId());
			}
		}

		return JsonResult.ok(createIfNotExists(siteId));
	}

}
