package com.uduemc.jcms.common.service;

import java.util.List;

import com.uduemc.jcms.common.entity.SiteLanguage;
import com.uduemc.jcms.generate.entity.JcmsSite;
import com.uduemc.jcms.generate.entity.JcmsSysLanguage;

public interface ISiteLanguageService {

	SiteLanguage defaultSiteLanguage();

	SiteLanguage findBySiteId(int siteId);

	SiteLanguage findByLanguageId(int languageId);

	SiteLanguage findBySite(JcmsSite site);

	SiteLanguage findByLanguage(JcmsSysLanguage language);

	List<SiteLanguage> findAll();

}
