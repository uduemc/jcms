package com.uduemc.jcms.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.uduemc.jcms.common.entity.RepertoryQuote;

/**
 * @author Uduemc
 * @since 2022-06-27
 */
@Mapper
public interface RepertoryQuoteMapper {

	RepertoryQuote selectByTypeAimId(@Param("siteId") int siteId, @Param("type") int type, @Param("aimId") int aimId);

	List<RepertoryQuote> selectListByTypeAimId(@Param("siteId") int siteId, @Param("type") int type, @Param("aimId") int aimId);
}
