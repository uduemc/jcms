package com.uduemc.jcms.common.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.uduemc.jcms.common.entity.Logo;

/**
 * @author Uduemc
 * @since 2022-06-27
 */
@Mapper
public interface LogoMapper {

	Logo selectBySiteId(@Param("siteId") int siteId);
}
