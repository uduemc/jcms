package com.uduemc.jcms.backend;

import org.springframework.stereotype.Component;

@Component
public class BackendModule {

	public final static String ModuleName = "backend";

	// 后台基础RequestMapper地址，也是backend模块拦截器的地址
	public final static String BaseRequest = "/" + BackendModule.ModuleName;

	// 后台登录地址
	public final static String LoginUri = BaseRequest + "/login.html";

	// 后台 ajax 提交登录地址
	public final static String LoginAjaxUri = BaseRequest + "/login-submit.html";

	// backend模块拦截器放行的地址
	public final static String[] ExcludePathPatterns = new String[] { LoginUri, LoginAjaxUri, BaseRequest + "/captcha-image.jpg" };

	// 后台登录用户存储 jcms_backend_user.id 的 session key
	public final static String BackendUserIdSession = "BackendUserIdSession";

	// 后台登录用户存储 JcmsBackendUser 的 session key
	public final static String BackendUserSession = "BackendUserSession";

	// 后台登录后存储 SiteLanguage 的 session key
	public final static String BackendSiteLanguageSession = "BackendSiteLanguageSession";

	public String uri(String path) {
		return BaseRequest + path;
	}
}
