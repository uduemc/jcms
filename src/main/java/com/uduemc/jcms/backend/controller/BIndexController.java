package com.uduemc.jcms.backend.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.uduemc.jcms.backend.config.BackendSidebarConfig;

@Controller
@RequestMapping("/backend")
public class BIndexController {

	@GetMapping(value = "/index.html")
	public ModelAndView index(ModelAndView model) throws IOException {
		model.addObject("sidebar", BackendSidebarConfig.getSidebar());
		model.addObject("bartree", BackendSidebarConfig.getBarTree());
		model.setViewName("backend/index");
		return model;
	}

	@GetMapping(value = "/welcome.html")
	public String welcome() throws IOException {
		return "backend/welcome";
	}
}
