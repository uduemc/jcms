package com.uduemc.jcms.backend.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.uduemc.jcms.config.category.ContentCategory;

@Controller
@RequestMapping("/backend/content")
public class BContentController {

	@GetMapping(value = "/category.html")
	public ModelAndView index(ModelAndView model) throws IOException {

		model.addObject("category_type_table", ContentCategory.typeTable);
		model.addObject("category_type", ContentCategory.type.def);

		model.setViewName("backend/content/category");
		return model;
	}
}
