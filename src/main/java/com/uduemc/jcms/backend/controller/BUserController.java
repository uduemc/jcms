package com.uduemc.jcms.backend.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.uduemc.jcms.backend.BackendModule;
import com.uduemc.jcms.backend.service.IBJcmsBackendUserService;
import com.uduemc.jcms.common.utils.JsonResult;

@Controller
@RequestMapping(BackendModule.BaseRequest + "/user")
public class BUserController {

	@Autowired
	IBJcmsBackendUserService bjcmsBackendUserServiceImpl;

	@GetMapping(value = "/list.html")
	public ModelAndView list(ModelAndView model) throws IOException {
		model.setViewName("backend/user/list");
		return model;
	}

	@GetMapping(value = "/list-infos.html")
	@ResponseBody
	public JsonResult listInfos(@RequestParam(value = "page") int page, @RequestParam(value = "rows") int rows) throws IOException {
		return bjcmsBackendUserServiceImpl.listInfos(page, rows);
	}

	@GetMapping(value = "/resetpassword.html")
	public String resetpassword() throws IOException {
		return "backend/user/resetpassword";
	}

	@PostMapping(value = "/resetpassword-ajax.html")
	@ResponseBody
	public JsonResult ajaxResetpassword(@RequestParam("opassword") String opassword, @RequestParam("password") String password) {
		return bjcmsBackendUserServiceImpl.ajaxResetpassword(opassword, password);
	}
}
