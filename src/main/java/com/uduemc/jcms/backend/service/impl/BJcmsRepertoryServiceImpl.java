package com.uduemc.jcms.backend.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.uduemc.jcms.backend.component.BackendLoginForm;
import com.uduemc.jcms.backend.dto.RepertoryOutsidelinkSave;
import com.uduemc.jcms.backend.dto.RepertoryUpdate;
import com.uduemc.jcms.backend.service.IBJcmsRepertoryService;
import com.uduemc.jcms.backend.vo.JcmsRepertoryAimUsername;
import com.uduemc.jcms.common.service.IRepertoryQuoteService;
import com.uduemc.jcms.common.utils.PageDataUtil;
import com.uduemc.jcms.common.utils.PageDataUtil.PageData;
import com.uduemc.jcms.component.RepertoryUtil;
import com.uduemc.jcms.generate.entity.JcmsBackendUser;
import com.uduemc.jcms.generate.entity.JcmsRepertory;
import com.uduemc.jcms.generate.mapper.JcmsRepertoryMapper;
import com.uduemc.jcms.generate.service.IJcmsBackendUserService;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;

@Service("bjcmsRepertoryServiceImpl")
public class BJcmsRepertoryServiceImpl extends ServiceImpl<JcmsRepertoryMapper, JcmsRepertory> implements IBJcmsRepertoryService {

	@Autowired
	BackendLoginForm backendLoginForm;

	@Autowired
	JcmsRepertoryMapper jcmsRepertoryMapper;

	@Autowired
	IJcmsBackendUserService jcmsBackendUserServiceImpl;

	@Autowired
	IRepertoryQuoteService repertoryQuoteServiceImpl;

	@Autowired
	RepertoryUtil repertoryUtil;

	@Override
	public boolean saveOutsideLink(RepertoryOutsidelinkSave repertoryOutsidelink) {
		Long id = repertoryOutsidelink.getId();
		JcmsRepertory jcmsRepertory;
		if (id != null && id.longValue() > 0) {
			jcmsRepertory = getById(id);
			if (jcmsRepertory == null) {
				return false;
			}
			BeanUtil.copyProperties(repertoryOutsidelink, jcmsRepertory);
		} else {
			jcmsRepertory = BeanUtil.copyProperties(repertoryOutsidelink, JcmsRepertory.class);
			jcmsRepertory.setId(null);
			JcmsBackendUser backendUser = backendLoginForm.getUser();
			jcmsRepertory.setAimId(backendUser.getId());
			jcmsRepertory.setUserType(1);
			jcmsRepertory.setType(6);
			jcmsRepertory.setSuffix(jcmsRepertory.getOriginalFilename().lastIndexOf(".") >= 0
					? StrUtil.subSuf(jcmsRepertory.getOriginalFilename(), jcmsRepertory.getOriginalFilename().lastIndexOf(".") + 1)
					: "");
			jcmsRepertory.setUnidname(makeUnidname());
			jcmsRepertory.setFilepath("");
			jcmsRepertory.setZoompath("");
			jcmsRepertory.setSize(0L);
			jcmsRepertory.setPixel("");
			jcmsRepertory.setRefuse(0);
		}

		return saveOrUpdate(jcmsRepertory);
	}

	@Override
	public boolean update(RepertoryUpdate repertoryUpdate) {
		JcmsRepertory jcmsRepertory = getById(repertoryUpdate.getId());
		BeanUtil.copyProperties(repertoryUpdate, jcmsRepertory);
		return saveOrUpdate(jcmsRepertory);
	}

	@Override
	public PageData<JcmsRepertoryAimUsername> selectPageInfos(String originalFilename, List<Long> labelids, List<Long> types, int isRefuse, int page,
			int rows) {
		Page<JcmsRepertory> selectPage = new Page<>(page, rows);
		LambdaQueryWrapper<JcmsRepertory> queryWrapper = new LambdaQueryWrapper<>();
		if (StrUtil.isNotBlank(originalFilename)) {
			queryWrapper.like(JcmsRepertory::getOriginalFilename, originalFilename);
		}

		// 如果包含 -1 则不进行过滤
		Predicate<Long> predicate = ele -> {
			return ele.longValue() == -1;
		};
		if (CollUtil.isNotEmpty(labelids) && !CollUtil.contains(labelids, predicate)) {
			queryWrapper.in(JcmsRepertory::getLabelId, labelids);
		}
		if (CollUtil.isNotEmpty(types) && !CollUtil.contains(types, predicate)) {
			queryWrapper.in(JcmsRepertory::getType, types);
		}

		queryWrapper.eq(JcmsRepertory::getRefuse, isRefuse);
		queryWrapper.orderByDesc(JcmsRepertory::getId);
		Page<JcmsRepertory> infos = jcmsRepertoryMapper.selectPage(selectPage, queryWrapper);
		List<JcmsRepertory> records = infos.getRecords();
		long total = infos.getTotal();

		List<JcmsRepertoryAimUsername> list = new ArrayList<JcmsRepertoryAimUsername>(records.size());
		for (JcmsRepertory jcmsRepertory : records) {
			JcmsRepertoryAimUsername repertoryAimUsername = repertoryQuoteServiceImpl.makeJcmsRepertoryAimUsername(jcmsRepertory);
			list.add(repertoryAimUsername);
		}
		return PageDataUtil.easyuiResult(list, total, "userType", "aimId", "filepath", "zoompath", "isRefuse", "unidname");
	}

	@Override
	public String makeUnidname() {
		String unidname = IdUtil.fastSimpleUUID();
		JcmsRepertory selectOne = getByUnidname(unidname);
		if (selectOne == null) {
			return unidname;
		}
		return makeUnidname();
	}

	@Override
	public JcmsRepertory getByUnidname(String unidname) {
		LambdaQueryWrapper<JcmsRepertory> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(JcmsRepertory::getUnidname, unidname);
		return jcmsRepertoryMapper.selectOne(queryWrapper);
	}

}
