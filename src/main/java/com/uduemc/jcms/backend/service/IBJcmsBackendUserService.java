package com.uduemc.jcms.backend.service;

import com.uduemc.jcms.common.utils.JsonResult;
import com.uduemc.jcms.generate.entity.JcmsBackendUser;

/**
 * <p>
 * 后台登录 用户表 服务类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-07
 */
public interface IBJcmsBackendUserService {

	JcmsBackendUser findByUsername(String username);

	JsonResult listInfos(int page, int rows);

	JsonResult ajaxResetpassword(String opassword, String password);

	JsonResult loginSubmit(String captcha, String username, String password);
}
