package com.uduemc.jcms.backend.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.uduemc.jcms.generate.entity.JcmsRepertoryLabel;

public interface IBJcmsRepertoryLabelService extends IService<JcmsRepertoryLabel> {

	public List<JcmsRepertoryLabel> list();

	public Page<JcmsRepertoryLabel> labelInfos(int page, int rows);
}
