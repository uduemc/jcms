package com.uduemc.jcms.backend.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.uduemc.jcms.backend.component.BackendLoginForm;
import com.uduemc.jcms.backend.component.PasswordSecure;
import com.uduemc.jcms.backend.service.IBJcmsBackendUserService;
import com.uduemc.jcms.common.utils.JsonResult;
import com.uduemc.jcms.common.utils.PageDataUtil;
import com.uduemc.jcms.generate.entity.JcmsBackendUser;
import com.uduemc.jcms.generate.mapper.JcmsBackendUserMapper;
import com.uduemc.jcms.generate.service.IJcmsBackendUserService;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.StrUtil;

@Service("bjcmsBackendUserServiceImpl")
public class BJcmsBackendUserServiceImpl implements IBJcmsBackendUserService {

	@Autowired
	JcmsBackendUserMapper jcmsBackendUserMapper;

	@Autowired
	IJcmsBackendUserService jcmsBackendUserServiceImpl;

	@Autowired
	BackendLoginForm backendLoginForm;

	@Autowired
	PasswordSecure passwordSecure;

	@Autowired
	HttpServletRequest request;

	@Override
	public JcmsBackendUser findByUsername(String username) {
		LambdaQueryWrapper<JcmsBackendUser> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(JcmsBackendUser::getUsername, username);
		return jcmsBackendUserMapper.selectOne(queryWrapper);
	}

	@Override
	public JsonResult listInfos(int page, int rows) {
		Page<JcmsBackendUser> selectPage = new Page<>(page, rows);

		QueryWrapper<JcmsBackendUser> queryWrapper = new QueryWrapper<>();
		queryWrapper.orderByAsc("id");

		Page<JcmsBackendUser> infos = jcmsBackendUserServiceImpl.getBaseMapper().selectPage(selectPage, queryWrapper);
		return JsonResult.ok(PageDataUtil.easyuiResult(infos, "passwordHash", "status"));
	}

	@Override
	public JsonResult ajaxResetpassword(String opassword, String password) {
		if (StrUtil.isBlank(opassword) || StrUtil.isBlank(password)) {
			return JsonResult.illegal();
		}

		JcmsBackendUser backendUser = backendLoginForm.getUser();
		if (!passwordSecure.verifyHex(opassword, backendUser.getPasswordHash())) {
			return JsonResult.messageError("旧密码不正确！");
		}

		String digestHex = passwordSecure.backendUserPasswordHash(password);
		backendUser.setPasswordHash(digestHex);
		backendUser.setPassword(Base64.encode(password));
		boolean saveOrUpdate = jcmsBackendUserServiceImpl.saveOrUpdate(backendUser);
		if (!saveOrUpdate) {
			return JsonResult.assistance();
		}

		return JsonResult.messageSuccess("修改密码成功！", 1);
	}

	@Override
	public JsonResult loginSubmit(String captcha, String username, String password) {
		HttpSession session = request.getSession();
		String code = (String) session.getAttribute("captcha");
		if (StrUtil.isBlank(captcha) || !captcha.equals(code)) {
			return JsonResult.messageError("验证码不匹配！");
		}
		if (StrUtil.isBlank(username)) {
			return JsonResult.messageError("账号不能为空！");
		}

		if (StrUtil.isBlank(password)) {
			return JsonResult.messageError("密码不能为空！");
		}

		JcmsBackendUser jcmsBackendUser = findByUsername(username);
		if (jcmsBackendUser == null) {
			return JsonResult.messageError("账号或密码不正确！");
		}

		if (!passwordSecure.verifyHex(password, jcmsBackendUser.getPasswordHash())) {
			return JsonResult.messageError("账号或密码不正确！");
		}

		backendLoginForm.login(jcmsBackendUser);

		return JsonResult.ok(jcmsBackendUser.getId());
	}

}
