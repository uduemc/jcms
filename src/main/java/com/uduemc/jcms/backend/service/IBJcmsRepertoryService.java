package com.uduemc.jcms.backend.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.uduemc.jcms.backend.dto.RepertoryOutsidelinkSave;
import com.uduemc.jcms.backend.dto.RepertoryUpdate;
import com.uduemc.jcms.backend.vo.JcmsRepertoryAimUsername;
import com.uduemc.jcms.common.utils.PageDataUtil.PageData;
import com.uduemc.jcms.generate.entity.JcmsRepertory;

public interface IBJcmsRepertoryService extends IService<JcmsRepertory> {

	boolean saveOutsideLink(RepertoryOutsidelinkSave repertoryOutsidelink);

	boolean update(RepertoryUpdate repertoryUpdate);

	PageData<JcmsRepertoryAimUsername> selectPageInfos(String originalFilename, List<Long> labelids, List<Long> types, int isRefuse, int page, int rows);

	String makeUnidname();

	JcmsRepertory getByUnidname(String unidname);

}
