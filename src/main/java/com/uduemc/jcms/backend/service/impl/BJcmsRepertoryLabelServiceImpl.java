package com.uduemc.jcms.backend.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.uduemc.jcms.backend.service.IBJcmsRepertoryLabelService;
import com.uduemc.jcms.generate.entity.JcmsRepertoryLabel;
import com.uduemc.jcms.generate.mapper.JcmsRepertoryLabelMapper;
import com.uduemc.jcms.generate.service.IJcmsRepertoryLabelService;

@Service("bjcmsRepertoryLabelServiceImpl")
public class BJcmsRepertoryLabelServiceImpl extends ServiceImpl<JcmsRepertoryLabelMapper, JcmsRepertoryLabel> implements IBJcmsRepertoryLabelService {

	@Autowired
	IJcmsRepertoryLabelService jcmsRepertoryLabelServiceImpl;

	@Override
	public List<JcmsRepertoryLabel> list() {
		List<JcmsRepertoryLabel> list = jcmsRepertoryLabelServiceImpl.list();
		return list;
	}

	@Override
	public Page<JcmsRepertoryLabel> labelInfos(int page, int rows) {
		Page<JcmsRepertoryLabel> selectPage = new Page<>(page, rows);
		LambdaQueryWrapper<JcmsRepertoryLabel> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.orderByAsc(JcmsRepertoryLabel::getId);
		Page<JcmsRepertoryLabel> infos = jcmsRepertoryLabelServiceImpl.getBaseMapper().selectPage(selectPage, queryWrapper);
		return infos;
	}
}
