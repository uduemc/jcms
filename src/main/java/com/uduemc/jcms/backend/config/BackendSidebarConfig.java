package com.uduemc.jcms.backend.config;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cn.hutool.core.util.IdUtil;
import lombok.Data;

public class BackendSidebarConfig {

	ObjectMapper objectMapper;

	private static int sideId = 1;

	private static List<BarTree> conf = null;

	static {
		conf = new ArrayList<>();

		// 内容管理
		List<BarTree.Bar> contextSiteTree = new ArrayList<>();
		contextSiteTree.add(new BarTree.Bar("内容列表", "/backend/content/list.html"));
		contextSiteTree.add(new BarTree.Bar("内容分类", "/backend/content/category.html"));
		conf.add(new BarTree("内容管理", contextSiteTree));

		// 资源管理
		List<BarTree.Bar> repertorySiteTree = new ArrayList<>();
		repertorySiteTree.add(new BarTree.Bar("资源列表", "/backend/repertory/index.html"));
		repertorySiteTree.add(new BarTree.Bar("资源标签", "/backend/repertory/label.html"));
		repertorySiteTree.add(new BarTree.Bar("回收站", "/backend/repertory/recycle.html"));
		conf.add(new BarTree("资源管理", repertorySiteTree));

		// 整站管理
		List<BarTree.Bar> hostSiteTree = new ArrayList<>();
		hostSiteTree.add(new BarTree.Bar("网站Logo", "/backend/site/logo.html"));
		conf.add(new BarTree("整站管理", hostSiteTree));

		// 后台管理
		List<BarTree.Bar> backendSiteTree = new ArrayList<>();
		backendSiteTree.add(new BarTree.Bar("登录用户", "/backend/user/list.html"));
//		backendSiteTree.add(new BarTree.Bar("修改密码", repertorySiteTree));
		backendSiteTree.add(new BarTree.Bar("修改密码", "/backend/user/resetpassword.html"));
		conf.add(new BarTree("后台用户", backendSiteTree));

	}

	public static String getSidebar() throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		if (conf == null) {
			return null;
		}
		return objectMapper.writeValueAsString(conf);
	}

	public static List<BarTree> getBarTree() {
		return conf;
	}

	@Data
	public static class BarTree {
		String id;
		String name;
		List<Bar> bar;

		BarTree(String nam, List<Bar> ba) {
			this.name = nam;
			this.bar = ba;
			this.id = "bar-" + IdUtil.randomUUID();
		}

		@Data
		public static class Bar {
			String id;
			String iconCls = "icon-chart-organisation";
			String text = "";
			List<Bar> children;
			BarAttributes attributes;

			Bar(String text, String link) {
				this.id = String.valueOf(BackendSidebarConfig.sideId++);
				this.text = text;
				BarAttributes attributes = new BarAttributes(link);
				this.attributes = attributes;
			}

			Bar(String text, List<Bar> child) {
				this.id = String.valueOf(BackendSidebarConfig.sideId++);
				this.text = text;
				this.children = child;
			}

			@Data
			public static class BarAttributes {
				int iframe = 1;
				String link = "";

				BarAttributes(String link) {
					this.link = link;
				}
			}

		}
	}

}
