package com.uduemc.jcms.backend.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.uduemc.jcms.backend.validator.JcmsRepertoryLabelIdExist;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
public class RepertoryOutsidelinkSave {

	@NotNull(message = "ID不能为空")
	Long id;
	@NotBlank(message = "外链地址不能为空")
	String link;
	@NotBlank(message = "资源名称不能为空")
	String originalFilename;
	@NotNull(message = "LABEL_ID不能为空")
	@JcmsRepertoryLabelIdExist
	Long labelId;
}
