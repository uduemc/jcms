package com.uduemc.jcms.backend.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.uduemc.jcms.backend.BackendModule;
import com.uduemc.jcms.common.entity.SiteLanguage;
import com.uduemc.jcms.common.service.ISiteLanguageService;

@Component
public class BackendSiteLanguageInterceptor implements HandlerInterceptor {

	@Autowired
	ApplicationContext applicationContext;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HttpSession session = request.getSession();
		SiteLanguage attribute = (SiteLanguage) session.getAttribute(BackendModule.BackendSiteLanguageSession);

		if (attribute == null) {
			ISiteLanguageService siteLanguageServiceImpl = (ISiteLanguageService) applicationContext.getBean("siteLanguageServiceImpl");
			SiteLanguage defaultSiteLanguage = siteLanguageServiceImpl.defaultSiteLanguage();
			session.setAttribute(BackendModule.BackendSiteLanguageSession, defaultSiteLanguage);
		}

		return true;
	}
}
