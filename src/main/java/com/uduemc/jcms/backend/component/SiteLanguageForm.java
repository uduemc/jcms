package com.uduemc.jcms.backend.component;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.jcms.backend.BackendModule;
import com.uduemc.jcms.common.entity.SiteLanguage;

@Component
public class SiteLanguageForm {

	@Autowired
	HttpServletRequest request;

	public SiteLanguage getSiteLanguage() {
		SiteLanguage siteLanguage = (SiteLanguage) request.getSession().getAttribute(BackendModule.BackendSiteLanguageSession);
		if (siteLanguage != null) {
			return siteLanguage;
		}

		throw new RuntimeException("找不到 SiteLanguage 数据！");
	}
}
