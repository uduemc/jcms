package com.uduemc.jcms.backend.component;

import org.springframework.stereotype.Component;

import com.uduemc.jcms.common.utils.JcmsSmUtil;

import cn.hutool.core.codec.Base64;

@Component
public class PasswordSecure {

	/**
	 * 后台登录用户的密码加密
	 * 
	 * @param password
	 * @return
	 */
	public String backendUserPasswordHash(String password) {
		return JcmsSmUtil.encrypt(password);
	}

	/**
	 * 后台登录用户的 密码 Base64
	 * 
	 * @param password
	 * @return
	 */
	public String backendUserPassword(String password) {
		return Base64.encode(password);
	}

	/**
	 * 后台登录用户的密码验证
	 * 
	 * @param password
	 * @param hashCode
	 * @return
	 */
	public boolean verifyHex(String password, String hashCode) {
		String decrypt = JcmsSmUtil.decrypt(hashCode);
		return decrypt.equals(password);
	}
}
