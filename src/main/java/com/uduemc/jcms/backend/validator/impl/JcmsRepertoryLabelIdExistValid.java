package com.uduemc.jcms.backend.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.uduemc.jcms.backend.validator.JcmsRepertoryLabelIdExist;
import com.uduemc.jcms.generate.entity.JcmsRepertoryLabel;
import com.uduemc.jcms.generate.service.IJcmsRepertoryLabelService;

public class JcmsRepertoryLabelIdExistValid implements ConstraintValidator<JcmsRepertoryLabelIdExist, Long> {

	@Autowired
	ApplicationContext applicationContext;

	@Override
	public boolean isValid(Long value, ConstraintValidatorContext context) {
		if (value == null || value.longValue() < 1) {
			return true;
		}

		IJcmsRepertoryLabelService jcmsRepertoryLabelServiceImpl = applicationContext.getBean("jcmsRepertoryLabelServiceImpl",
				IJcmsRepertoryLabelService.class);

		JcmsRepertoryLabel repertoryLabel = jcmsRepertoryLabelServiceImpl.getById(value);

		if (repertoryLabel == null) {
			return false;
		}

		return true;
	}

}
