package com.uduemc.jcms.backend.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.uduemc.jcms.backend.service.IBJcmsRepertoryService;
import com.uduemc.jcms.backend.validator.JcmsRepertoryIdExist;
import com.uduemc.jcms.generate.entity.JcmsRepertory;

public class JcmsRepertoryIdExistValid implements ConstraintValidator<JcmsRepertoryIdExist, Long> {

	@Autowired
	ApplicationContext applicationContext;

	@Override
	public boolean isValid(Long value, ConstraintValidatorContext context) {
		if (value == null || value.longValue() < 1) {
			return true;
		}

		IBJcmsRepertoryService bjcmsRepertoryServiceImpl = applicationContext.getBean("bjcmsRepertoryServiceImpl", IBJcmsRepertoryService.class);

		JcmsRepertory repertory = bjcmsRepertoryServiceImpl.getById(value);

		if (repertory == null) {
			return false;
		}

		return true;
	}

}
