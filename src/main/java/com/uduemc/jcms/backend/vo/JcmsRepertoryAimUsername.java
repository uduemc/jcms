package com.uduemc.jcms.backend.vo;

import com.uduemc.jcms.generate.entity.JcmsRepertory;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class JcmsRepertoryAimUsername extends JcmsRepertory {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String aimUsertype = "";
	String aimUsername = "";
	String imageSrc = "";
	String linkUrl = "";
	String downloadUrl = "";

}
