package com.uduemc.jcms.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.uduemc.jcms.entity.Categories;
import com.uduemc.jcms.service.ICategoriesService;
import com.uduemc.jcms.validator.CategoriesIdExist;

public class CategoriesIdExistValid implements ConstraintValidator<CategoriesIdExist, Integer> {

	@Autowired
	ApplicationContext applicationContext;

	@Override
	public boolean isValid(Integer value, ConstraintValidatorContext context) {
		if (value == null || value.longValue() < 1) {
			return true;
		}

		ICategoriesService categoriesServiceImpl = applicationContext.getBean("categoriesServiceImpl",
				ICategoriesService.class);

		Categories categories = categoriesServiceImpl.getById(value);

		if (categories == null) {
			return false;
		}

		return true;
	}

}
