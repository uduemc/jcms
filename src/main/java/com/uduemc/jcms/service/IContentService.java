package com.uduemc.jcms.service;

import com.uduemc.jcms.entity.Content;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 整体系统内容数据 服务类
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
public interface IContentService extends IService<Content> {

}
