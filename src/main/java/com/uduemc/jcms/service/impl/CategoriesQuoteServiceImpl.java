package com.uduemc.jcms.service.impl;

import com.uduemc.jcms.entity.CategoriesQuote;
import com.uduemc.jcms.mapper.CategoriesQuoteMapper;
import com.uduemc.jcms.service.ICategoriesQuoteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 多对多分类引用数据 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
@Service
public class CategoriesQuoteServiceImpl extends ServiceImpl<CategoriesQuoteMapper, CategoriesQuote> implements ICategoriesQuoteService {

}
