package com.uduemc.jcms.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.uduemc.jcms.common.utils.PageDataUtil;
import com.uduemc.jcms.common.utils.PageDataUtil.PageData;
import com.uduemc.jcms.entity.Categories;
import com.uduemc.jcms.mapper.CategoriesMapper;
import com.uduemc.jcms.service.ICategoriesService;
import com.uduemc.jcms.vo.CategorySave;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

/**
 * <p>
 * 整体系统分类 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
@Service
public class CategoriesServiceImpl extends ServiceImpl<CategoriesMapper, Categories> implements ICategoriesService {

	@Autowired
	CategoriesMapper categoriesMapper;

	@Override
	public PageData<Categories> selectPageInfos(String typeTable, int type, int parentId, String name, String path, int page, int rows) {
		LambdaQueryWrapper<Categories> queryWrapper = new LambdaQueryWrapper<>();
		if (StrUtil.isNotBlank(typeTable)) {
			queryWrapper.eq(Categories::getTypeTable, typeTable);
		}

		queryWrapper.eq(Categories::getType, type);

		if (parentId > -1) {
			queryWrapper.eq(Categories::getParentId, parentId);
		}

		if (StrUtil.isNotBlank(name)) {
			queryWrapper.like(Categories::getName, name);
		}

		if (StrUtil.isNotBlank(path)) {
			queryWrapper.likeRight(Categories::getPath, path);
		}

		queryWrapper.orderByAsc(Categories::getParentId);
		queryWrapper.orderByAsc(Categories::getOrderNum);

		Page<Categories> selectPage = new Page<>(page, rows);

		Page<Categories> infos = categoriesMapper.selectPage(selectPage, queryWrapper);

		return PageDataUtil.easyuiResult(infos);
	}

	@Override
	public PageData<Categories> selectDeepInfos(String typeTable, int type, int deep) {
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		arrayList.add(0);
		return selectInfoByParentIdDeep(typeTable, type, arrayList, deep, 1);
	}

	@Override
	public PageData<Categories> selectInfoByParentIdDeep(String typeTable, int type, List<Integer> parentId, int deep, int index) {
		LambdaQueryWrapper<Categories> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(Categories::getTypeTable, typeTable);
		queryWrapper.eq(Categories::getType, type);
		queryWrapper.in(Categories::getParentId, parentId);

		queryWrapper.orderByAsc(Categories::getParentId);
		queryWrapper.orderByAsc(Categories::getOrderNum);
		List<Categories> selectList = categoriesMapper.selectList(queryWrapper);

		if (deep == index) {
			return PageDataUtil.easyuiResult(selectList);
		}
		selectList.forEach(action -> {
			parentId.add(action.getId());
		});

		return selectInfoByParentIdDeep(typeTable, type, parentId, deep, index + 1);
	}

	@Override
	public boolean save(CategorySave categorySave) {
		Integer id = categorySave.getId();
		Categories categories;
		if (id.intValue() < 1) {
			categories = new Categories();
			categories.setTypeTable(categorySave.getTypeTable());
			categories.setType(categorySave.getType());
			categories.setName(categorySave.getName());
			categories.setParentId(categorySave.getParentId());

			Categories parentCategories = getById(categorySave.getParentId());
			String path = parentCategories == null ? "" : parentCategories.getPath();

			ArrayList<String> arrayList = new ArrayList<>();
			Collections.addAll(arrayList, path.split(","));
			arrayList.add(String.valueOf(categorySave.getParentId()));

			String join = CollUtil.join(arrayList, ",");
			categories.setPath(join);
			categories.setOrderNum(categorySave.getOrderNum());

		} else {
			categories = getById(id);
			if (categories == null) {
				return false;
			}
			categories.setName(categorySave.getName());
			categories.setParentId(categorySave.getParentId());

			Categories parentCategories = getById(categorySave.getParentId());
			String path = parentCategories == null ? "" : parentCategories.getPath();

			ArrayList<String> arrayList = new ArrayList<>();
			Collections.addAll(arrayList, path.split(","));
			arrayList.add(String.valueOf(categorySave.getParentId()));

			String join = CollUtil.join(arrayList, ",");
			categories.setPath(join);
			categories.setOrderNum(categorySave.getOrderNum());
		}

		return save(categories);
	}

}
