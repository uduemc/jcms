package com.uduemc.jcms.service.impl;

import com.uduemc.jcms.entity.Content;
import com.uduemc.jcms.mapper.ContentMapper;
import com.uduemc.jcms.service.IContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 整体系统内容数据 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
@Service
public class ContentServiceImpl extends ServiceImpl<ContentMapper, Content> implements IContentService {

}
