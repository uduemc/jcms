package com.uduemc.jcms.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.uduemc.jcms.common.utils.PageDataUtil.PageData;
import com.uduemc.jcms.entity.Categories;
import com.uduemc.jcms.vo.CategorySave;

/**
 * <p>
 * 整体系统分类 服务类
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
public interface ICategoriesService extends IService<Categories> {

	PageData<Categories> selectPageInfos(String typeTable, int type, int parentId, String name, String path, int page, int rows);

	PageData<Categories> selectDeepInfos(String typeTable, int type, int deep);

	PageData<Categories> selectInfoByParentIdDeep(String typeTable, int type, List<Integer> parentId, int deep, int index);

	boolean save(CategorySave categorySave);
}
