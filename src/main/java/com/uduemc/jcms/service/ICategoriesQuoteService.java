package com.uduemc.jcms.service;

import com.uduemc.jcms.entity.CategoriesQuote;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 多对多分类引用数据 服务类
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
public interface ICategoriesQuoteService extends IService<CategoriesQuote> {

}
