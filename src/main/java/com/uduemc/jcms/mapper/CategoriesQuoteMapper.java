package com.uduemc.jcms.mapper;

import com.uduemc.jcms.entity.CategoriesQuote;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 多对多分类引用数据 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
public interface CategoriesQuoteMapper extends BaseMapper<CategoriesQuote> {

}
