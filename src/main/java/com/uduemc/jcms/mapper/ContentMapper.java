package com.uduemc.jcms.mapper;

import com.uduemc.jcms.entity.Content;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 整体系统内容数据 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
public interface ContentMapper extends BaseMapper<Content> {

}
