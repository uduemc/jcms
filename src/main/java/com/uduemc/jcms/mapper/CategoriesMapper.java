package com.uduemc.jcms.mapper;

import com.uduemc.jcms.entity.Categories;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 整体系统分类 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2021-05-12
 */
public interface CategoriesMapper extends BaseMapper<Categories> {

}
