package com.uduemc.jcms.dto;

import com.uduemc.jcms.entity.Repertory;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class RepertoryAimUsername extends Repertory {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String aimUsertype = "";
	String aimUsername = "";
	String imageSrc = "";
	String linkUrl = "";
	String downloadUrl = "";
}
