package com.uduemc.jcms.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.uduemc.jcms.common.entity.SiteLanguage;
import com.uduemc.jcms.common.service.ISiteLanguageService;

@Component
public class SiteLanguageInterceptor implements HandlerInterceptor {

	@Autowired
	ApplicationContext applicationContext;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		SiteLanguage siteLanguage = (SiteLanguage) request.getAttribute("siteLanguage");
		if (siteLanguage == null) {
			ISiteLanguageService siteLanguageServiceImpl = (ISiteLanguageService) applicationContext.getBean("siteLanguageServiceImpl");
			SiteLanguage defaultSiteLanguage = siteLanguageServiceImpl.defaultSiteLanguage();
			request.setAttribute("siteLanguage", defaultSiteLanguage);
		}
		return true;
	}
}
