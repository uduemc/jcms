package com.uduemc.jcms.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.uduemc.jcms.backend.BackendModule;
import com.uduemc.jcms.backend.interceptor.BackendLoginInterceptor;
import com.uduemc.jcms.backend.interceptor.BackendSiteLanguageInterceptor;
import com.uduemc.jcms.interceptor.SiteLanguageInterceptor;

@Configuration
public class InterceptorConfiguration implements WebMvcConfigurer {

	@Autowired
	SiteLanguageInterceptor siteLanguageInterceptor;

	@Autowired
	BackendLoginInterceptor backendLoginInterceptor;

	@Autowired
	BackendSiteLanguageInterceptor backendSiteLanguageInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 后台登录校验
		registry.addInterceptor(backendLoginInterceptor).addPathPatterns(BackendModule.BaseRequest + "/**")
				.excludePathPatterns(BackendModule.ExcludePathPatterns);
		// 确定后台站点语言版本
		registry.addInterceptor(backendSiteLanguageInterceptor).addPathPatterns(BackendModule.BaseRequest + "/**")
				.excludePathPatterns(BackendModule.ExcludePathPatterns);

		// 前台
		registry.addInterceptor(siteLanguageInterceptor).addPathPatterns("/**").excludePathPatterns(BackendModule.BaseRequest + "/**")
				.excludePathPatterns(BackendModule.ExcludePathPatterns);

		WebMvcConfigurer.super.addInterceptors(registry);
	}
}
