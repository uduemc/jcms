package com.uduemc.jcms.vo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.uduemc.jcms.validator.CategoriesIdExist;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Accessors(chain = true)
public class CategorySave {

	@NotNull(message = "ID不能为空")
	@CategoriesIdExist
	Integer id;

	@NotNull(message = "ParentID不能为空")
	@CategoriesIdExist
	Integer parentId;

	@NotBlank(message = "分类名称不能为空")
	String name;

	@NotBlank(message = "分类TypeTable不能为空")
	String typeTable;

	@NotNull(message = "分类Type不能为空")
	Integer type;

	@NotNull(message = "分类Type不能为空")
	Integer orderNum;
}
