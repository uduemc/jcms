package com.uduemc.jcms.config.render;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.uduemc.jcms.config.basic.JcmsBasicConfig;

import lombok.Data;

@Data
@Component(value = "jcmsRenderConfig")
@ConfigurationProperties(prefix = "jcms.render")
public class JcmsRenderConfig {

	String entrance = "render";

	@Autowired
	JcmsBasicConfig jcmsBasicConfig;

	public String filepath() {
		return jcmsBasicConfig.getPath() + File.separator + this.getEntrance();
	}
}
