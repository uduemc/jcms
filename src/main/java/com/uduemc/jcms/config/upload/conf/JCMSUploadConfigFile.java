package com.uduemc.jcms.config.upload.conf;

import lombok.Data;

@Data
public class JCMSUploadConfigFile {
	String name = "file";
	int type = 2;
	boolean enabled = true;
	String ext[] = { "pdf", "xla", "xls", "xlsx", "xlt", "xlw", "doc", "docx", "txt", "zip", "rar", "tar", "gz", "7z",
			"bz2", "ppt", "pptx", ".xmind", ".md", ".xml" };
	JCMSUploadConfigFileSize rulSize = new JCMSUploadConfigFileSize();
}
