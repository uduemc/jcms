package com.uduemc.jcms.config.upload.conf;

import lombok.Data;

@Data
public class JCMSUploadConfigAudio {
	String name = "audio";
	int type = 5;
	boolean enabled = true;
	String ext[] = { "mp3" };
	JCMSUploadConfigAudioSize rulSize = new JCMSUploadConfigAudioSize();
}
