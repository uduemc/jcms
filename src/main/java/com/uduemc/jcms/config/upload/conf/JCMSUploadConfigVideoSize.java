package com.uduemc.jcms.config.upload.conf;

import com.uduemc.jcms.config.upload.JcmsUploadConfig;

import lombok.Data;

@Data
public class JCMSUploadConfigVideoSize implements IUploadConfigRulSize {

	private static long defaultSize = 200 * 1024 * 1024L;

	private String value = "200M";
	private String message = "视频文件上传大小不能超过{{value}}";

	public long validSize() {
		return JcmsUploadConfig.makeValidSize(value, defaultSize);
	}

	public String errorMessage() {
		return this.message.replaceAll("\\{\\{value\\}\\}", this.value);
	}
}
