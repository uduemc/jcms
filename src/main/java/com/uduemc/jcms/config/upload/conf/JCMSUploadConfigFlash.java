package com.uduemc.jcms.config.upload.conf;

import lombok.Data;

@Data
public class JCMSUploadConfigFlash {
	String name = "flash";
	int type = 3;
	boolean enabled = true;
	String ext[] = { "swf" };
	JCMSUploadConfigFlashSize rulSize = new JCMSUploadConfigFlashSize();
}
