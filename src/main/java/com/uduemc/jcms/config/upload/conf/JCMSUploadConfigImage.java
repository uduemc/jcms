package com.uduemc.jcms.config.upload.conf;

import lombok.Data;

@Data
public class JCMSUploadConfigImage {
	String name = "image";
	int type = 1;
	boolean enabled = true;
	String ext[] = { "png", "jpg", "jpeg", "gif", "bmp", "ico" };
	JCMSUploadConfigImageSize rulSize = new JCMSUploadConfigImageSize();
}
