package com.uduemc.jcms.config.upload.conf;

import lombok.Data;

@Data
public class JCMSUploadConfigVideo {
	String name = "video";
	int type = 4;
	boolean enabled = true;
	String ext[] = { "mp4" };
	JCMSUploadConfigVideoSize rulSize = new JCMSUploadConfigVideoSize();
}
