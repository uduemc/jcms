package com.uduemc.jcms.config.upload.conf;

interface IUploadConfigRulSize {

	long validSize();

	String errorMessage();
}
