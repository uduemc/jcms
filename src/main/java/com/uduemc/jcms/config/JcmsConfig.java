package com.uduemc.jcms.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.jcms.config.basic.JcmsBasicConfig;
import com.uduemc.jcms.config.render.JcmsRenderConfig;
import com.uduemc.jcms.config.upload.JcmsUploadConfig;

@Component
public class JcmsConfig {

	@Autowired
	JcmsBasicConfig jcmsBasicConfig;

	@Autowired
	JcmsUploadConfig jcmsUploadConfig;

	@Autowired
	JcmsRenderConfig jcmsRenderConfig;
}
