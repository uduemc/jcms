package com.uduemc.jcms.config.basic;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component(value = "jcmsBasicConfig")
@ConfigurationProperties(prefix = "jcms.basic")
public class JcmsBasicConfig {
	String path = "/jcms/source";
}
