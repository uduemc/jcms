package com.uduemc.jcms.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class NotFoundRepertoryException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String filePath;
	String unidname;

	public NotFoundRepertoryException(String message) {
		super(message);
	}

	public NotFoundRepertoryException(String message, String filePath) {
		super(message);
		this.setFilePath(filePath);
	}

	public NotFoundRepertoryException(String message, String filePath, String unidname) {
		super(message);
		this.setFilePath(filePath);
		this.setUnidname(unidname);
	}
}
