package com.uduemc.jcms.generate.service.impl;

import com.uduemc.jcms.generate.entity.JcmsSCategoryQuote;
import com.uduemc.jcms.generate.mapper.JcmsSCategoryQuoteMapper;
import com.uduemc.jcms.generate.service.IJcmsSCategoryQuoteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsSCategoryQuoteServiceImpl extends ServiceImpl<JcmsSCategoryQuoteMapper, JcmsSCategoryQuote> implements IJcmsSCategoryQuoteService {

}
