package com.uduemc.jcms.generate.service.impl;

import com.uduemc.jcms.generate.entity.JcmsBackendUser;
import com.uduemc.jcms.generate.mapper.JcmsBackendUserMapper;
import com.uduemc.jcms.generate.service.IJcmsBackendUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台登录 用户表 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsBackendUserServiceImpl extends ServiceImpl<JcmsBackendUserMapper, JcmsBackendUser> implements IJcmsBackendUserService {
	@Autowired
	JcmsBackendUserMapper jcmsBackendUserMapper;

	@Override
	public JcmsBackendUser findByUsername(String username) {
		LambdaQueryWrapper<JcmsBackendUser> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(JcmsBackendUser::getUsername, username);
		return jcmsBackendUserMapper.selectOne(queryWrapper);
	}
}
