package com.uduemc.jcms.generate.service.impl;

import com.uduemc.jcms.generate.entity.JcmsSysLanguage;
import com.uduemc.jcms.generate.mapper.JcmsSysLanguageMapper;
import com.uduemc.jcms.generate.service.IJcmsSysLanguageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsSysLanguageServiceImpl extends ServiceImpl<JcmsSysLanguageMapper, JcmsSysLanguage> implements IJcmsSysLanguageService {

}
