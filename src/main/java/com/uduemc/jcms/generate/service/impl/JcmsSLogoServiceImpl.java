package com.uduemc.jcms.generate.service.impl;

import com.uduemc.jcms.generate.entity.JcmsSLogo;
import com.uduemc.jcms.generate.mapper.JcmsSLogoMapper;
import com.uduemc.jcms.generate.service.IJcmsSLogoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 站点Logo配置表 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsSLogoServiceImpl extends ServiceImpl<JcmsSLogoMapper, JcmsSLogo> implements IJcmsSLogoService {

}
