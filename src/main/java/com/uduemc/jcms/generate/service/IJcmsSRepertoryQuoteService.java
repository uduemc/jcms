package com.uduemc.jcms.generate.service;

import com.uduemc.jcms.generate.entity.JcmsSRepertoryQuote;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 资源引用表 服务类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface IJcmsSRepertoryQuoteService extends IService<JcmsSRepertoryQuote> {

}
