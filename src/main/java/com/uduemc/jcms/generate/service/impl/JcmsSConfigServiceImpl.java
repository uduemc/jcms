package com.uduemc.jcms.generate.service.impl;

import com.uduemc.jcms.generate.entity.JcmsSConfig;
import com.uduemc.jcms.generate.mapper.JcmsSConfigMapper;
import com.uduemc.jcms.generate.service.IJcmsSConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 站点配置表 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsSConfigServiceImpl extends ServiceImpl<JcmsSConfigMapper, JcmsSConfig> implements IJcmsSConfigService {

}
