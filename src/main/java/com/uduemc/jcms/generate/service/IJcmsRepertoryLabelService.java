package com.uduemc.jcms.generate.service;

import com.uduemc.jcms.generate.entity.JcmsRepertoryLabel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 资源模块标签表 服务类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface IJcmsRepertoryLabelService extends IService<JcmsRepertoryLabel> {

}
