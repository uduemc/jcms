package com.uduemc.jcms.generate.service;

import com.uduemc.jcms.generate.entity.JcmsBackendUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台登录 用户表 服务类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface IJcmsBackendUserService extends IService<JcmsBackendUser> {

	JcmsBackendUser findByUsername(String username);

}
