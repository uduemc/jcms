package com.uduemc.jcms.generate.service.impl;

import com.uduemc.jcms.generate.entity.JcmsSPage;
import com.uduemc.jcms.generate.mapper.JcmsSPageMapper;
import com.uduemc.jcms.generate.service.IJcmsSPageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 导航菜单 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsSPageServiceImpl extends ServiceImpl<JcmsSPageMapper, JcmsSPage> implements IJcmsSPageService {

}
