package com.uduemc.jcms.generate.service.impl;

import com.uduemc.jcms.generate.entity.JcmsCategoryType;
import com.uduemc.jcms.generate.mapper.JcmsCategoryTypeMapper;
import com.uduemc.jcms.generate.service.IJcmsCategoryTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 全站分类系统总表 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsCategoryTypeServiceImpl extends ServiceImpl<JcmsCategoryTypeMapper, JcmsCategoryType> implements IJcmsCategoryTypeService {

}
