package com.uduemc.jcms.generate.service;

import com.uduemc.jcms.generate.entity.JcmsSysLanguage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface IJcmsSysLanguageService extends IService<JcmsSysLanguage> {

}
