package com.uduemc.jcms.generate.service;

import com.uduemc.jcms.generate.entity.JcmsSPage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 导航菜单 服务类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface IJcmsSPageService extends IService<JcmsSPage> {

}
