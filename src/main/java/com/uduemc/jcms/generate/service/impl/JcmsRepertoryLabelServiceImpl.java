package com.uduemc.jcms.generate.service.impl;

import com.uduemc.jcms.generate.entity.JcmsRepertoryLabel;
import com.uduemc.jcms.generate.mapper.JcmsRepertoryLabelMapper;
import com.uduemc.jcms.generate.service.IJcmsRepertoryLabelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资源模块标签表 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsRepertoryLabelServiceImpl extends ServiceImpl<JcmsRepertoryLabelMapper, JcmsRepertoryLabel> implements IJcmsRepertoryLabelService {

}
