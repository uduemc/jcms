package com.uduemc.jcms.generate.service.impl;

import com.uduemc.jcms.generate.entity.JcmsSCategory;
import com.uduemc.jcms.generate.mapper.JcmsSCategoryMapper;
import com.uduemc.jcms.generate.service.IJcmsSCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 分类 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsSCategoryServiceImpl extends ServiceImpl<JcmsSCategoryMapper, JcmsSCategory> implements IJcmsSCategoryService {

}
