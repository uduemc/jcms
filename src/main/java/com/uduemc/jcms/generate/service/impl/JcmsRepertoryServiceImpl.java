package com.uduemc.jcms.generate.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.uduemc.jcms.generate.entity.JcmsRepertory;
import com.uduemc.jcms.generate.mapper.JcmsRepertoryMapper;
import com.uduemc.jcms.generate.service.IJcmsRepertoryService;

/**
 * <p>
 * 资源模块表 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsRepertoryServiceImpl extends ServiceImpl<JcmsRepertoryMapper, JcmsRepertory> implements IJcmsRepertoryService {

	@Autowired
	JcmsRepertoryMapper jcmsRepertoryMapper;

	@Override
	public JcmsRepertory getByUnidname(String unidname) {
		LambdaQueryWrapper<JcmsRepertory> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(JcmsRepertory::getUnidname, unidname);
		return jcmsRepertoryMapper.selectOne(queryWrapper);
	}
}
