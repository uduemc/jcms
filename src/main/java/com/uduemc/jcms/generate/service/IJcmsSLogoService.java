package com.uduemc.jcms.generate.service;

import com.uduemc.jcms.generate.entity.JcmsSLogo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 站点Logo配置表 服务类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface IJcmsSLogoService extends IService<JcmsSLogo> {

}
