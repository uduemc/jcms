package com.uduemc.jcms.generate.service.impl;

import com.uduemc.jcms.generate.entity.JcmsSite;
import com.uduemc.jcms.generate.mapper.JcmsSiteMapper;
import com.uduemc.jcms.generate.service.IJcmsSiteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 站点表 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsSiteServiceImpl extends ServiceImpl<JcmsSiteMapper, JcmsSite> implements IJcmsSiteService {

}
