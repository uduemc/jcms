package com.uduemc.jcms.generate.service;

import com.uduemc.jcms.generate.entity.JcmsCategoryType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 全站分类系统总表 服务类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface IJcmsCategoryTypeService extends IService<JcmsCategoryType> {

}
