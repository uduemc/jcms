package com.uduemc.jcms.generate.service.impl;

import com.uduemc.jcms.generate.entity.JcmsSRepertoryQuote;
import com.uduemc.jcms.generate.mapper.JcmsSRepertoryQuoteMapper;
import com.uduemc.jcms.generate.service.IJcmsSRepertoryQuoteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资源引用表 服务实现类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Service
public class JcmsSRepertoryQuoteServiceImpl extends ServiceImpl<JcmsSRepertoryQuoteMapper, JcmsSRepertoryQuote> implements IJcmsSRepertoryQuoteService {

}
