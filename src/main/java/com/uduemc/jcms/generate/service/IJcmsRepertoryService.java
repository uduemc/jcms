package com.uduemc.jcms.generate.service;

import com.uduemc.jcms.generate.entity.JcmsRepertory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 资源模块表 服务类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface IJcmsRepertoryService extends IService<JcmsRepertory> {

	JcmsRepertory getByUnidname(String unidname);
}
