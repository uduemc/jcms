package com.uduemc.jcms.generate.service;

import com.uduemc.jcms.generate.entity.JcmsSCategoryQuote;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface IJcmsSCategoryQuoteService extends IService<JcmsSCategoryQuote> {

}
