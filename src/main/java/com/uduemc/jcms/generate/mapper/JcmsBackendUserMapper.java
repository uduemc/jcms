package com.uduemc.jcms.generate.mapper;

import com.uduemc.jcms.generate.entity.JcmsBackendUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 后台登录 用户表 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface JcmsBackendUserMapper extends BaseMapper<JcmsBackendUser> {

}
