package com.uduemc.jcms.generate.mapper;

import com.uduemc.jcms.generate.entity.JcmsSysLanguage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface JcmsSysLanguageMapper extends BaseMapper<JcmsSysLanguage> {

}
