package com.uduemc.jcms.generate.mapper;

import com.uduemc.jcms.generate.entity.JcmsSite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 站点表 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface JcmsSiteMapper extends BaseMapper<JcmsSite> {

}
