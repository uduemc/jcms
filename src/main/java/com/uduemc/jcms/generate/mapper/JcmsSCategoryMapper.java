package com.uduemc.jcms.generate.mapper;

import com.uduemc.jcms.generate.entity.JcmsSCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分类 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface JcmsSCategoryMapper extends BaseMapper<JcmsSCategory> {

}
