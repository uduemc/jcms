package com.uduemc.jcms.generate.mapper;

import com.uduemc.jcms.generate.entity.JcmsSConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 站点配置表 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface JcmsSConfigMapper extends BaseMapper<JcmsSConfig> {

}
