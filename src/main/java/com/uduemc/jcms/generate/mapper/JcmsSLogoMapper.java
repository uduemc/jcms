package com.uduemc.jcms.generate.mapper;

import com.uduemc.jcms.generate.entity.JcmsSLogo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 站点Logo配置表 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface JcmsSLogoMapper extends BaseMapper<JcmsSLogo> {

}
