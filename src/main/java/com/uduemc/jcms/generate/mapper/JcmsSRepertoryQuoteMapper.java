package com.uduemc.jcms.generate.mapper;

import com.uduemc.jcms.generate.entity.JcmsSRepertoryQuote;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 资源引用表 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface JcmsSRepertoryQuoteMapper extends BaseMapper<JcmsSRepertoryQuote> {

}
