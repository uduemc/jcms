package com.uduemc.jcms.generate.mapper;

import com.uduemc.jcms.generate.entity.JcmsRepertory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 资源模块表 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface JcmsRepertoryMapper extends BaseMapper<JcmsRepertory> {

}
