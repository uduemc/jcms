package com.uduemc.jcms.generate.mapper;

import com.uduemc.jcms.generate.entity.JcmsSPage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 导航菜单 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface JcmsSPageMapper extends BaseMapper<JcmsSPage> {

}
