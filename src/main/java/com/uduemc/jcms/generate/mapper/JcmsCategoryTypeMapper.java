package com.uduemc.jcms.generate.mapper;

import com.uduemc.jcms.generate.entity.JcmsCategoryType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 全站分类系统总表 Mapper 接口
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
public interface JcmsCategoryTypeMapper extends BaseMapper<JcmsCategoryType> {

}
