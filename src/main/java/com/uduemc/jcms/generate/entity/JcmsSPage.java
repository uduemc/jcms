package com.uduemc.jcms.generate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 导航菜单
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("jcms_s_page")
public class JcmsSPage implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 站点ID,如果引导页，site_id必为0
     */
    private Integer siteId;

    /**
     * 父菜单ID
     */
    private Integer parentId;

    /**
     * 系统ID
     */
    private Integer systemId;

    /**
     * 访问权限 默认 -1
     */
    private Integer vipLevel;

    /**
     * 页面类型 0-引导页 1-普通页 2-系统页 3-外链页 4-内链页
     */
    private Integer type;

    /**
     * 名称
     */
    private String name;

    /**
     * 状态:0:不显示，1：显示
     */
    private Integer status;

    /**
     * 重写链接
     */
    private String rewrite;

    /**
     * 链接
     */
    private String url;

    /**
     * 跳转方式
     */
    private String target;

    /**
     * 引导页配置
     */
    private String config;

    /**
     * 排序号
     */
    private Integer orderNum;

    /**
     * 创建日期
     */
    private LocalDateTime createAt;


}
