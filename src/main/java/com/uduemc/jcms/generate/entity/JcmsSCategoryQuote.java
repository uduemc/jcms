package com.uduemc.jcms.generate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("jcms_s_category_quote")
public class JcmsSCategoryQuote implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 唯一标识
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 分类类型ID
     */
    private Integer categoryTypeId;

    /**
     * 站点ID
     */
    private Integer siteId;

    /**
     * 分类ID
     */
    private Integer categoryId;

    /**
     * 目标ID
     */
    private Integer aimId;

    /**
     * 系统ID
     */
    private Integer orderNum;

    /**
     * 创建时间
     */
    private LocalDateTime createAt;


}
