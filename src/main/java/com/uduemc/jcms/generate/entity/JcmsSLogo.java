package com.uduemc.jcms.generate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 站点Logo配置表
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("jcms_s_logo")
public class JcmsSLogo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 语言ID
     */
    private Integer siteId;

    /**
     * 0-无logo 1-文字 2-图片
     */
    private Integer type;

    /**
     * Logo 文字
     */
    private String title;

    /**
     * logo 配置属性
     */
    private String config;

    /**
     * 创建时间
     */
    private LocalDateTime createAt;


}
