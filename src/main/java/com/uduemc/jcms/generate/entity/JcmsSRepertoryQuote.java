package com.uduemc.jcms.generate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 资源引用表
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("jcms_s_repertory_quote")
public class JcmsSRepertoryQuote implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 语言ID
     */
    private Integer siteId;

    /**
     * 父级ID
     */
    private Integer parentId;

    /**
     * 资源ID
     */
    private Integer repertoryId;

    /**
     * 类型(1:Logo,2:favorites图标)
     */
    private Integer type;

    /**
     * 目标ID
     */
    private Integer aimId;

    /**
     * 排序号
     */
    private Integer orderNum;

    /**
     * 配置信息
     */
    private String config;

    /**
     * 创建时间
     */
    private LocalDateTime createAt;


}
