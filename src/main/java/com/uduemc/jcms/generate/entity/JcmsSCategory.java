package com.uduemc.jcms.generate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 分类
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("jcms_s_category")
public class JcmsSCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 站点ID
     */
    private Integer siteId;

    /**
     * 分类类型
     */
    private Integer categoryTypeId;

    /**
     * 父id
     */
    private Integer parentId;

    /**
     * 分类路径，使用[,]分隔，顶级为空
     */
    private String path;

    /**
     * 分类名称
     */
    private String name;

    /**
     * 重写链接
     */
    private String rewrite;

    /**
     * 排序号
     */
    private Integer orderNum;

    /**
     * 创建时间
     */
    private LocalDateTime createAt;


}
