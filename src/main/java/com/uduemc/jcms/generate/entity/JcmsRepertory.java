package com.uduemc.jcms.generate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 资源模块表
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("jcms_repertory")
public class JcmsRepertory implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 上传操作用户 1-后台
     */
    private Integer userType;

    /**
     * 根据 user_type 类型记录的对应用户ID
     */
    private Integer aimId;

    /**
     * 标签ID（0为未有标签）
     */
    private Integer labelId;

    /**
     * 资源类型 1-图片 2-文件 3-Flash 4-视频 5-音频 6-外链图片 7-SSL -1-未知
     */
    private Integer type;

    /**
     * 真实文件名称
     */
    private String originalFilename;

    /**
     * 外部资源链接地址
     */
    private String link;

    /**
     * 后缀
     */
    private String suffix;

    /**
     * 上传过来的 ContentType
     */
    private String contentType;

    /**
     * 上传文件的唯一码，只对当前整站
     */
    private String unidname;

    /**
     * 文件存储物理路径（相对路径）
     */
    private String filepath;

    /**
     * 缩放文件存储路径（相对路径）
     */
    private String zoompath;

    /**
     * 大小
     */
    private Long size;

    /**
     * 像素(图片使用)
     */
    private String pixel;

    /**
     * 0:非回收站；1：在回收站
     */
    private Integer refuse;

    /**
     * 上传日期
     */
    private LocalDateTime createAt;


}
