package com.uduemc.jcms.generate.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 站点配置表
 * </p>
 *
 * @author Uduemc
 * @since 2022-06-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("jcms_s_config")
public class JcmsSConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 站点ID
     */
    private Integer siteId;

    /**
     * 是否开启整站统一banner 0-否
     */
    private Integer siteBanner;

    /**
     * 是否使用整站同步 SEO 0-否 1-是 默认1
     */
    private Integer siteSeo;

    /**
     * 是否是主站点 0-不是 1-是 默认 0
     */
    private Integer isMain;

    /**
     * 排序
     */
    private Integer orderNum;

    /**
     * 创建时间
     */
    private LocalDateTime createAt;


}
