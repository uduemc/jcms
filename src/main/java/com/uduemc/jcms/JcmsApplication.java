package com.uduemc.jcms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan({ "com.uduemc.jcms.mapper", "com.uduemc.jcms.generate.mapper", "com.uduemc.jcms.common.mapper" })
@SpringBootApplication()
public class JcmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(JcmsApplication.class, args);
	}

}
