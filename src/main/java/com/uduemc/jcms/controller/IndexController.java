package com.uduemc.jcms.controller;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.hutool.core.io.resource.Resource;
import cn.hutool.core.io.resource.ResourceUtil;

@Controller
public class IndexController {

	@GetMapping("/favicon.ico")
	@ResponseBody
	public void favicon(HttpServletResponse response) throws IOException {
		Resource resourceObj = ResourceUtil.getResourceObj("static/favicon.ico");
		try (OutputStream outputStream = response.getOutputStream();) {
			response.setContentType("image/x-icon");
			byte[] readBytes = resourceObj.readBytes();
			outputStream.write(readBytes);
			outputStream.flush();
		}
		return;
	}

	@GetMapping("/")
	public ModelAndView index(ModelAndView modelAndView) {
		modelAndView.setViewName("sinoorchard/index.html");
		return modelAndView;
	}

}
