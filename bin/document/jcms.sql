-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： 127.0.0.1
-- 生成日期： 2021-04-09 18:48:11
-- 服务器版本： 5.7.23-log
-- PHP 版本： 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `jcms`
--

-- --------------------------------------------------------

--
-- 表的结构 `backend_user`
--

CREATE TABLE `backend_user` (
  `id` int(20) UNSIGNED NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '用户名',
  `password_hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '密码',
  `phone` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '电话',
  `email` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '邮箱',
  `status` int(5) DEFAULT '0' COMMENT '状态',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `theme` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT 'default' COMMENT '主题'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `backend_user`
--

INSERT INTO `backend_user` (`id`, `username`, `password_hash`, `phone`, `email`, `status`, `created_at`, `updated_at`, `theme`) VALUES
(1, 'admin', '6b149f9e0e35275edc0ef49419f1010384b437ad927388598ef5949b91b0c766', '18250850586', 'uduemc@163.com', 10, '2020-08-20 16:30:32', '2020-08-21 14:34:20', 'default');

-- --------------------------------------------------------

--
-- 表的结构 `repertory`
--

CREATE TABLE `repertory` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `user_type` int(11) NOT NULL DEFAULT '1' COMMENT '上传操作用户 1-后台',
  `aim_id` int(11) NOT NULL COMMENT '根据 user_type 类型记录的对应用户ID',
  `label_id` int(11) UNSIGNED DEFAULT '0' COMMENT '标签ID（0为未有标签）',
  `type` smallint(6) NOT NULL COMMENT '资源类型 1-图片 2-文件 3-Flash 4-视频 5-音频 6-外链图片 -1-未知',
  `original_filename` varchar(255) NOT NULL COMMENT '真实文件名称',
  `link` varchar(255) DEFAULT '' COMMENT '外部资源链接地址',
  `suffix` varchar(255) DEFAULT '' COMMENT '后缀',
  `content_type` varchar(127) DEFAULT '' COMMENT '上传过来的 ContentType',
  `unidname` varchar(255) DEFAULT '' COMMENT '上传文件的唯一码，只对当前整站',
  `filepath` varchar(255) DEFAULT '' COMMENT '文件存储物理路径（相对路径）',
  `zoompath` varchar(255) DEFAULT '' COMMENT '缩放文件存储路径（相对路径）',
  `size` bigint(20) DEFAULT '0' COMMENT '大小',
  `pixel` varchar(50) DEFAULT '0' COMMENT '像素(图片使用)',
  `is_refuse` smallint(1) DEFAULT '0' COMMENT '0:非回收站；1：在回收站',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上传日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源模块表';

--
-- 转存表中的数据 `repertory`
--

INSERT INTO `repertory` (`id`, `user_type`, `aim_id`, `label_id`, `type`, `original_filename`, `link`, `suffix`, `content_type`, `unidname`, `filepath`, `zoompath`, `size`, `pixel`, `is_refuse`, `created_at`) VALUES
(78, 1, 1, 29, 6, 'VCG211164834529.jpg', 'https://goss2.cfp.cn/creative/vcg/800/new/VCG211164834529.jpg', '', '', '4cef0e93ce524a0dbfdcd66a1ccdbae8', '', '', 0, '', 0, '2021-04-01 17:06:40'),
(79, 1, 1, 0, 6, 'VCG211164358460.jpg', 'https://goss3.cfp.cn/creative/vcg/800/new/VCG211164358460.jpg', '', '', '929c4a896fde4c6fbd891f1ab6305f65', '', '', 0, '', 0, '2021-04-01 17:07:13'),
(80, 1, 1, 0, 6, 'up-80a89217bcadc895f502b230f462060597a.png', 'https://oscimg.oschina.net/oscnet/up-80a89217bcadc895f502b230f462060597a.png', 'png', '', '2390d03a6f7e4aa6bc66644f72a10d49', '', '', 0, '', 0, '2021-04-01 17:19:46'),
(81, 1, 1, 0, 6, 'W020141224709548258414.jpg', 'http://acad.cssn.cn/zx/shwx/shhnew/201412/W020141224709548258414.jpg', 'jpg', '', '91dcea8505134ff9b4bda8027faafe8c', '', '', 0, '', 0, '2021-04-02 11:53:16'),
(82, 1, 1, 0, 6, 'W020141224709548533874.jpg', 'http://acad.cssn.cn/zx/shwx/shhnew/201412/W020141224709548533874.jpg', 'jpg', '', '0de1df86bb6e45bea25f46302f8891ec', '', '', 0, '', 0, '2021-04-02 11:53:33'),
(83, 1, 1, 0, 6, 'VCG211228610767.jpg', 'https://goss2.cfp.cn/creative/vcg/800/new/VCG211228610767.jpg', 'jpg', '', '3f460dfcca264fd980062a3642f816aa', '', '', 0, '', 0, '2021-04-02 14:24:44'),
(84, 1, 1, 0, 6, '1.jpg', 'https://goss2.cfp.cn/creative/vcg/800/new/VCG211228610767.jpg', 'jpg', '', '60464075bcc34b9fb66790c5a95b2173', '', '', 0, '', 0, '2021-04-02 14:24:52'),
(85, 1, 1, 0, 6, '3.jpg', 'https://goss4.cfp.cn/creative/vcg/800/new/VCG211295079928.jpg', 'jpg', '', '82bf826589ad4219986ce263cff4ff13', '', '', 0, '', 0, '2021-04-02 14:30:36'),
(86, 1, 1, 0, 6, 'VCG211296854039.jpg', 'https://goss.cfp.cn/creative/vcg/800/new/VCG211296854039.jpg', 'jpg', '', '8898c68e92394441bee2b5fae96ddd6b', '', '', 0, '', 0, '2021-04-02 14:38:32'),
(87, 1, 1, 0, 6, 'VCG211300411836.jpg', 'https://goss.cfp.cn/creative/vcg/800/new/VCG211300411836.jpg', 'jpg', '', '520070578f3b4c0e84d557d1d02097a3', '', '', 0, '', 0, '2021-04-02 14:38:44'),
(88, 1, 1, 0, 6, 'VCG211300477515.jpg', 'https://goss1.cfp.cn/creative/vcg/800/new/VCG211300477515.jpg', 'jpg', '', 'a1f06232e719420586545a24ba777966', '', '', 0, '', 0, '2021-04-02 14:39:02'),
(89, 1, 1, 0, 6, 'VCG211298153248.jpg', 'https://goss.cfp.cn/creative/vcg/800/new/VCG211298153248.jpg', 'jpg', '', '10bd1c62c30a427f94c5d4642d3e68eb', '', '', 0, '', 0, '2021-04-02 14:39:51'),
(90, 1, 1, 0, 6, 'VCG211293859300.jpg', 'https://goss1.cfp.cn/creative/vcg/800/new/VCG211293859300.jpg', 'jpg', '', '0457a1138d3e40a98436fa1490c75577', '', '', 0, '', 0, '2021-04-02 14:40:07'),
(91, 1, 1, 0, 6, 'VCG211300574370.jpg', 'https://goss4.cfp.cn/creative/vcg/800/new/VCG211300574370.jpg', 'jpg', '', 'cda2e2fa63054e3ca0e5ec8cec7a5804', '', '', 0, '', 0, '2021-04-02 14:40:30'),
(92, 1, 1, 0, 5, '取消订单_耳聆网_[声音ID：35836](3).mp3', '', 'mp3', 'audio/mpeg', 'e445e35d06cb44948d08195ff505fdff', '\\repertory\\2021\\4\\7\\取消订单_耳聆网_[声音ID：35836](3).mp3', '\\repertory\\2021\\4\\7\\e445e35d06cb44948d08195ff505fdff', 79661, '', 0, '2021-04-07 14:09:26'),
(93, 1, 1, 0, 5, '新订单_耳聆网_[声音ID：35835](1).mp3', '', 'mp3', 'audio/mpeg', 'd164d87bee49404ab57f0c72bd06f32e', '\\repertory\\2021\\4\\7\\新订单_耳聆网_[声音ID：35835](1).mp3', '\\repertory\\2021\\4\\7\\d164d87bee49404ab57f0c72bd06f32e', 87713, '', 0, '2021-04-07 16:02:25'),
(94, 1, 1, 0, 1, 'J.png', '', 'png', 'image/png', '8170aad61efe46018fbfb5779af9e6bc', '\\repertory\\2021\\4\\7\\J.png', '\\repertory\\2021\\4\\7\\8170aad61efe46018fbfb5779af9e6bc', 23679, '500X500', 0, '2021-04-07 16:02:32'),
(95, 1, 1, 0, 1, 'J.png', '', 'png', 'image/png', '0f20a56c90304a7a85340167a1f759bd', '\\repertory\\2021\\4\\7\\J.png', '\\repertory\\2021\\4\\7\\0f20a56c90304a7a85340167a1f759bd', 23679, '500X500', 0, '2021-04-07 16:04:30'),
(97, 1, 1, 0, 1, 'J.png', '', 'png', 'image/png', '251b6dfa6c394d54a9f0cc37cf4de65e', '\\repertory\\2021\\4\\7\\J.png', '\\repertory\\2021\\4\\7\\251b6dfa6c394d54a9f0cc37cf4de65e', 23679, '500X500', 0, '2021-04-07 16:23:57'),
(98, 1, 1, 0, 1, 'menuimg_fix1.png', '', 'png', 'image/png', '58f0118271054088b0a61b6fbdf047e4', '\\repertory\\2021\\4\\8\\58f0118271054088b0a61b6fbdf047e4.png', '\\repertory\\2021\\4\\8\\58f0118271054088b0a61b6fbdf047e4', 3101, '129X97', 0, '2021-04-08 14:51:45'),
(99, 1, 1, 0, 1, 'menuimg_fix2.png', '', 'png', 'image/png', 'b2f7d1a2e0344d6ba2e865b7543419de', '\\repertory\\2021\\4\\8\\b2f7d1a2e0344d6ba2e865b7543419de.png', '\\repertory\\2021\\4\\8\\b2f7d1a2e0344d6ba2e865b7543419de', 1995, '131X97', 0, '2021-04-08 14:54:45'),
(100, 1, 1, 0, 4, '97dgwHV5rh1s1528084264.mp4', '', 'mp4', 'video/mp4', 'a7256da01dae445293d4b09e90b89ca7', '\\repertory\\2021\\4\\8\\a7256da01dae445293d4b09e90b89ca7.mp4', '\\repertory\\2021\\4\\8\\a7256da01dae445293d4b09e90b89ca7', 1258245, '', 0, '2021-04-08 14:58:11'),
(101, 1, 1, 0, 2, '35OpenAPI.pdf', '', 'pdf', 'application/pdf', '41ff090948d740eeb4867bef78599345', '\\repertory\\2021\\4\\8\\41ff090948d740eeb4867bef78599345.pdf', '\\repertory\\2021\\4\\8\\41ff090948d740eeb4867bef78599345', 580979, '', 0, '2021-04-08 14:58:20');

-- --------------------------------------------------------

--
-- 表的结构 `repertory_label`
--

CREATE TABLE `repertory_label` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL COMMENT '标签名',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源模块标签表';

--
-- 转存表中的数据 `repertory_label`
--

INSERT INTO `repertory_label` (`id`, `name`, `created_at`) VALUES
(28, '人物', '2021-03-30 11:28:43'),
(29, '描绘', '2021-03-30 11:29:38'),
(30, '风景', '2021-03-30 11:29:48'),
(31, 'aaa', '2021-03-30 11:37:30'),
(32, 'abc', '2021-03-30 11:37:34'),
(33, 'eee', '2021-03-30 11:37:48'),
(34, 'fff', '2021-03-30 11:37:51'),
(35, 'ggg', '2021-03-30 11:37:54'),
(36, 'hhh', '2021-03-30 11:37:56'),
(37, 'iii', '2021-03-30 11:37:59'),
(38, 'jjj', '2021-03-30 11:38:02'),
(39, 'kkk4', '2021-03-30 11:38:06');

--
-- 转储表的索引
--

--
-- 表的索引 `backend_user`
--
ALTER TABLE `backend_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- 表的索引 `repertory`
--
ALTER TABLE `repertory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `label_id` (`label_id`),
  ADD KEY `unidname` (`unidname`),
  ADD KEY `filepath` (`filepath`),
  ADD KEY `original_filename` (`original_filename`);

--
-- 表的索引 `repertory_label`
--
ALTER TABLE `repertory_label`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `backend_user`
--
ALTER TABLE `backend_user`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `repertory`
--
ALTER TABLE `repertory`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=102;

--
-- 使用表AUTO_INCREMENT `repertory_label`
--
ALTER TABLE `repertory_label`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
