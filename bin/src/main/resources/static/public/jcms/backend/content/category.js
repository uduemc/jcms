$(function() {
	// 执行easyui渲染
	$('#content-def-categories').treegrid({
		title : '内容分类（双击数据列编辑）',
		toolbar : '#content-def-categories-tb',
		singleSelect : true,
		width : 600,
		minHeight : 250,
		url : contentDefCategoriesUrl,
		method : 'GET',
		pagination : true,
		queryParams : {
			type_table : function() {
				return $('#category-type_table').textbox('getValue');
			},
			type : function() {
				return $('#category-type').textbox('getValue');
			}
		},
		columns : [ [ {
			field : 'id',
			title : 'ID',
			width : 40,
			align : 'center'
		}, {
			field : 'name',
			title : '分类',
			width : 130,
			align : 'center'
		}, {
			field : 'createdAt',
			title : '创建时间',
			width : 120,
			align : 'center',
			formatter : function(val, row, ind) {
				return moment(val).format('YYYY-MM-DD');
			}
		} ] ],
		onClickRow : function(index, field, value) {
			window.event ? window.event.cancelBubble = true : e.stopPropagation();
		},
		onDblClickCell : function(index, field, value) {
			var rows = $('#repertory-label').datagrid('getRows');
			var row = rows[index];
			clickEdtLabel(row.id, row.name);
			window.event ? window.event.cancelBubble = true : e.stopPropagation();
		},
		loadFilter : function(data) {
			if (data && data.data) {
				return data.data;
			}
			return {
				rows : []
			};
		}
	});

	$('#category-parent_id').combotree({
		data : [ {
			"id" : 1,
			"text" : "顶级",
		} ]
	});

	// 展示页面
	$('body').mLoading('hide');

	// 事件

});

function clickAdd() {
	$(function() {
		$('#category-id').textbox("setValue", "");
		$('#category-name').textbox("setValue", "");
		var options = $('#category-parent_id').combotree('options');
		options.data = [ {
			"id" : 0,
			"text" : "顶级",
		} ];
		$('#category-parent_id').combotree(options);
		$('#category-order_num').numberbox("setValue", "1");
		$('#category-dialog').dialog({
			title : '新增分类',
		}).dialog('open');
		$('#category-dialog').mLoading('show');
		window.globalData.categoryInfos({
			type_table : function() {
				return $('#category-type_table').textbox('getValue');
			},
			type : function() {
				return $('#category-type').textbox('getValue');
			}
		}, function(data) {
			if (data && data.rows && data.rows.length > 0) {
				var data = data.rows;
				console.log(data);
			}

			$('#category-parent_id').combotree('setValue', 0);
		}, function() {
			$('#category-dialog').mLoading('hide');
		})
	});

}

function doSubmit() {
	$(function() {
		var id = $.trim($('#category-id').textbox("getValue"));
		var parent_id = $.trim($('#category-parent_id').textbox("getValue"));
		var name = $.trim($('#category-name').textbox("getValue"));
		var order_num = $.trim($('#category-order_num').numberbox("getValue"));

		if (name.length < 1) {
			mylayer.warning('分类名不能为空！');
			return;
		}

		var param = {
			id : id ? id : -1,
			parentId : parent_id,
			name : name,
			typeTable : $('#category-type_table').textbox('getValue'),
			type : $('#category-type').textbox('getValue'),
			orderNum : order_num
		};

		request.jpost({
			url : contentDefCategoriesSaveUrl,
			data : param,
			success : function(rsp, data) {
				if (data && parseInt(data) === 1) {
					$('#content-def-categories').datagrid('reload');
					$('#category-dialog').dialog('close');
				}
			}
		});
	});
}