$(function() {
	// 执行easyui渲染
	$('#user-list').datagrid({
		title : '用户列表',
		width : 700,
		minHeight : 250,
		url : userListUrl,
		method : 'GET',
		pagination : true,
		columns : [ [ {
			field : 'id',
			title : 'ID',
			width : 40,
			align : 'center'
		}, {
			field : 'username',
			title : '登录用户名',
			width : 130,
			align : 'center'
		}, {
			field : 'phone',
			title : '联系电话',
			width : 140,
			align : 'center'
		}, {
			field : 'email',
			title : 'Email',
			width : 200,
			align : 'center'
		}, {
			field : 'createdAt',
			title : '创建时间',
			width : 120,
			align : 'center',
			formatter: function(val, row, ind){
				return moment(val).format('YYYY-MM-DD');
			}
		} ] ],
		loadFilter : function(data) {
			if (data && data.data) {
				return data.data;
			}
			return {
				rows : []
			};
		}
	});

	// 展示页面
	$('body').mLoading('hide');

	// 事件

});
