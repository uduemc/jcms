window.globalData = window.globalData || (function() {
	var globalUrl = {
		repertoryLabelInfosAllUrl: $('input[name="globalUrl-repertoryLabelInfosAllUrl"]').val()
	}
	return {
		repertoryLabelInfosData: {
			data: null,
			requesting: false
		},
		// 获取资源标签公用方法
		repertoryLabelInfos: function(callback) {
			var self = this;
			if (self.repertoryLabelInfosData.data) {
				if (typeof callback === 'function') {
					callback(self.repertoryLabelInfosData.data);
				}
				return;
			}
			// 是否在请求
			if (self.repertoryLabelInfosData.requesting) {
				// 循环等待
				setTimeout(function() {
					self.repertoryLabelInfos(callback);
				}, 100);
				return;
			}
			self.repertoryLabelInfosData.requesting = true;
			request.post({
				url: globalUrl.repertoryLabelInfosAllUrl,
				success: function(rsp, data) {
					self.repertoryLabelInfosData.data = data;
					self.repertoryLabelInfosData.requesting = false;
					self.repertoryLabelInfos(callback);
				}
			});
		},
		// 获取资源数据公共方法
		repertoryInfos: function(param, callback) {
			request.get({
				url: globalUrl.repertoryInfosUrl,
				data: param,
				success: function(rsp, data) {
					callback(data);
				}
			});
		},
		// 获取分类数据的方法
		categoryInfos: function(param, callback, completecallback) {
			request.get({
				url: globalUrl.categoryInfosUrl,
				data: param,
				success: function(rsp, data) {
					if (typeof callback === 'function') {
						callback(data);
					}
				},
				complete: function() {
					if (typeof completecallback === 'function') {
						completecallback();
					}
				}
			});
		}
	}
})()