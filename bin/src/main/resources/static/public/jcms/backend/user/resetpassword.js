$(function() {
	// 执行easyui渲染

	// 展示页面
	$('body').mLoading('hide');

	// 事件

});

function submitForm() {
	$(function() {
		var bool = $('#ff-reset-password').form('enableValidation').form('validate');
		if (bool) {
			// 
			var opassword = $.trim($("#opassword").passwordbox('getValue'));
			var password = $.trim($("#password").passwordbox('getValue'));
			var rpassword = $.trim($("#rpassword").passwordbox('getValue'));

			if (password !== rpassword) {
				mylayer.warning('两次输入的密码不一致！');
				return;
			}

			if (password.length < 4) {
				mylayer.warning('新密码长度大于4位！');
				return;
			}

			if (opassword == password) {
				mylayer.warning('修改的密码不能与旧密码一致！');
				return;
			}

			var param = {
				opassword : opassword,
				password : password
			}

			request.post({
				url : resetpasswordUrl,
				data : param
			});
		}
	});
}