$(function() {
	// 点击登录
	$('#doLogin').bind('click', function() {
		var username = $("#username").textbox('getValue');
		var password = $("#password").passwordbox('getValue');
		var captcha = $("#captcha").textbox('getValue');

		var bool = $('#loginform').form('enableValidation').form('validate');
		if (!bool) {
			return;
		}
		request.post({
			url : loginUrl,
			data : {
				username : username,
				password : password,
				captcha : captcha,
			},
			success : function(response, data) {
				if (data === 1) {
					window.location.href = homeUrl;
				}
			}
		});
	});

});